<?php

use App\Kullanici;
use App\User;
use Illuminate\Database\Seeder;

class KullaniciSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user =new User();
        $user->name="Admin";
        $user->email="admin@kainatenerjisi.com";
        $user->password=bcrypt('admin221133');
        $user->role='SuperAdmin';
        $user->save();

        $kullanici =new Kullanici();
        $kullanici->user_id=1;
        $kullanici->lastActive= date('Y-m-d H:i:s');
        $kullanici->save();
    }
}
