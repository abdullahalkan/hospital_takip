<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHastasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hastas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('adi');
            $table->string('telefon');
            $table->string('aile');
            $table->bigInteger('sube_id');
            $table->bigInteger('hoca_id');
            $table->bigInteger('user_id');
            $table->string('durum');
            $table->string('grup');
            $table->datetime('arama');
            $table->datetime('aranan');
            $table->boolean('arandi')->default(false);
            $table->string('yapilacaklar');
            $table->float('ucret');
            $table->float('alinan_ucret');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hastas');
    }
}
