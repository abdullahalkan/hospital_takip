<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHastaTemizlikTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hasta_temizlik', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('hasta_id');
            $table->bigInteger('temizlik_id');
            $table->string('tarih');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hasta_temizlik');
    }
}
