<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBakimsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bakims', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('adi');
            $table->string('telefon');
            $table->bigInteger('sube_id');
            $table->bigInteger('hoca_id');
            $table->enum('bakildimi',["Evet","Hayır"]);
            $table->enum('tedavikabul',["Evet","Hayır","Düşünüyor"]);
            $table->text('aciklama')->nullable();
            $table->text('sonuc')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bakims');
    }
}
