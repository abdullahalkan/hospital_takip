<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kullanici extends Model
{
    protected $fillable = ['durum','user_id','lastActive','hoca_id'];

    public function user(){
        return $this->belongsTo(User::class);
    }
    public function hoca(){
        return $this->belongsTo(Kullanici::class);
    }

}

