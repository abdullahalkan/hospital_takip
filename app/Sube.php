<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sube extends Model
{
    //
    protected $fillable=['adi'];

    public function hocas(){
        return $this->hasMany(Hoca::class);
    }
    public function hastas(){
        return $this->hasMany(hasta::class);
    }
}
