<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class hasta extends Model
{
    protected $fillable=['adi','telefon','aile','sube_id','hoca_id','grup','durum','arama','yapilacaklar','ucret','alinan_ucret','user_id','aranan','arandi'];
    public function hoca(){
        return $this->belongsTo(Hoca::class);
    }
    public function sube(){
        return $this->belongsTo(Sube::class);
    }
    public function odemes(){
        return $this->hasMany(Odeme::class);
    }
    public function okumas(){
        return $this->belongsToMany(Okuma::class)->withPivot('tarih')->withTimestamps();
    }
    public function temizliks(){
        return $this->belongsToMany(Temizlik::class)->withPivot('tarih')->withTimestamps();
    }
    public function dokumans(){
        return $this->belongsToMany(Dokuman::class)->withPivot('file')->withTimestamps();
    }
    public function nots(){
        return $this->hasMany(Not::class);
    }
    public function isArama(){

        $today = new Carbon('now');
        $tomorrow = new Carbon('now');
        $tomorrow = $tomorrow->add(1,'day');
//        $today=$today->format('d-m-Y H:i');
//        $tomorrow=$tomorrow->format('d-m-Y H:i');
//        if ((date('d-m-Y H:i',strtotime($this->arama)) <= $today) && (date('d-m-Y H:i',strtotime($this->arama)) >= $tomorrow) ){
        if ($this->arama >= $today && $this->arama <=$tomorrow){
            return true;
        }
        else return false;
    }
//    public function getArandiAttribute(){
//        return $this->arandi;
//    }
}
