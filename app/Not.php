<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Not extends Model
{
    protected $fillable = ['text','hasta_id'];
    public function hasta(){
        return $this->belongsTo(hasta::class);
    }
}
