<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Odeme extends Model
{
    protected $fillable = ['hasta_id','tutar','para_birimi','deger'];
    public function hasta(){
        return $this->belongsTo(hasta::class);
    }
}
