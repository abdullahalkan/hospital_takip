<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Temizlik extends Model
{
    protected $fillable =['adi'];
    public function hastas(){
        return $this->belongsToMany(hasta::class)->withPivot('tarih')->withTimestamps();
    }
}
