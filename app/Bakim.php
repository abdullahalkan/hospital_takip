<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bakim extends Model
{
    protected $fillable = ['adi','sube_id','hoca_id','bakildimi','tedavikabul','aciklama','sonuc','telefon'];
}
