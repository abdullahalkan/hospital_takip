<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hoca extends Model
{
    protected $fillable =['adi','sube_id','telefon'];
    public function sube(){
        return $this->belongsTo(Sube::class);
    }
    public function hastas(){
        return $this->hasMany(hasta::class);
    }

    public function kullanici(){
        return $this->belongsTo(Kullanici::class);
    }
}

