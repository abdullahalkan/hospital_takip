<?php

namespace App\Http\Controllers;

use App\Bakim;
use App\Hoca;
use App\Http\Requests\KullnaciCreateRequest;
use App\Kullanici;
use App\Sube;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\DataTables;

class KullaniciController extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return  view('backend.Kullanicilar.kullanicilar');
    }


    public function datatable()
    {
        $data = Kullanici::latest()->get();
        return Datatables::of($data)
            ->addColumn('action',function ($row){
                $btn ='<div class="d-flex" >
                   <a href="#" class="delete btn btn-outline-light text-warning btn-sm" id="modal" data-action-target="/kullanici/'.$row->id.'/delete" onclick="onay(this)"><i class="fa fa-trash"></i></a>
                   <a href="/kullanici/'.$row->id.'/edit" class="edit btn btn-outline-light text-success btn-sm"><i class="fa fa-edit"></i></a>            
                   <a href="/kullanici/'.$row->id.'/superadmin" class="btn btn-outline-primary ">Admin Yap</a>            
                     
                </div>';
                return $btn;
            })
            ->editColumn('email', function ($row){
               $user= User::find($row->user_id);
                return $user->email;
            })
            ->editColumn('name', function ($row){
                $user= User::find($row->user_id);
                return $user->name;
            })
            ->make(true);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return  view('backend.Kullanicilar.kullanici-ekle');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */


    public function store(KullnaciCreateRequest $request)
    {
        $data=$request->only('name','email','password');

        $user= User::create(
          [
              'name'=> $data['name'],
              'email' =>$data['email'],
              'password' =>bcrypt($data['password'])
         ]
        );

       if ( $user -> kullanici()->create([
           'durum' =>"Aktif",
           'user_id' =>$user->id,
           'lastActive'=> date('Y-m-d H:i:s')
       ])){
           session()->flash('success','Yeni kullanıcı eklendi');
           return redirect(route('kullanici.index'));
       }
       else{
           session()->flash('error','Yeni kullanıcı eklenemedi.Bilgileri kontrol ediniz.');
           return redirect(route('kullanici.index'));
       }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Kullanici  $kullanici
     * @return \Illuminate\Http\Response
     */
    public function show(Kullanici $kullanici)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Kullanici  $kullanici
     * @return \Illuminate\Http\Response
     */
    public function edit(Kullanici $kullanici)
    {
        return view('backend.Kullanicilar.kullanici-ekle')->with('kullanici',$kullanici);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Kullanici  $kullanici
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Kullanici $kullanici)
    {
        if ($kullanici->user->update([
            'name'=>$request->name,
            'email'=>$request->email,
            'password' => bcrypt($request->password),
        ])){
            session()->flash('success',' Kullanıcı düzenlendi');
            return redirect(route('kullanici.index'));
        }
        else{
            session()->flash('error','Kullanıcı düzenlenemedi.Bilgileri kontrol ediniz.');
            return redirect(route('kullanici.index'));
        }

    }
    public function superadmin($id){
        $user = User::find($id);
        $user->role = 'SuperAdmin';
        if ($user->save()){
        session()->flash('success',' Kullanıcı yetkilendirme yapıldı');
        return redirect(route('kullanici.index'));
    }
        else{
        session()->flash('error','Kullanıcı düzenlenemedi.Bilgileri kontrol ediniz.');
        return redirect(route('kullanici.index'));
}
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Kullanici  $kullanici
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kullanici=Kullanici::find($id);
        if($kullanici->user()->delete()){
            $kullanici->delete();
            session()->flash('success',' Kullanıcı silindi');
            return redirect(route('kullanici.index'));
        }
        else{
            session()->flash('error','Kullanıcı silinemedi.Bilgileri kontrol ediniz.');
            return redirect(route('kullanici.index'));
        }
    }
}
