<?php

namespace App\Http\Controllers;

use App\hasta;
use App\Hoca;
use App\Sube;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class SubelerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      return view('backend.Sube.Subeler');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.Sube.Sube-Create');
    }
    public function datatable()
    {
        $data = Sube::latest()->get();
        return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function($row){
                $btn ='<div class="d-flex" >
                   <a href="#" class="delete btn btn-outline-light text-warning btn-sm" id="modal" title="Sil"  data-action-target="/subeler/'.$row->id.'" onclick="onay(this)"><i class="fa fa-trash"></i></a>
                   <a href="/subeler/'.$row->id.'/edit" class="edit btn btn-outline-light text-success btn-sm" title="Düzenle"><i class="fa fa-edit"></i></a>
                   <a href="/subeler/'.$row->id.'/hocalar" class="edit btn btn-primary text-white ml-3 btn-sm " title="Bu şubedeki hocaları görüntüle"><i class="fa fa-address-card"></i></a>
                </div>';
                return $btn;
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->only('adi','durum');
        $sube = new Sube();
        $sube->adi = $data['adi'];
        $sube->durum = $data['durum'];
        if ($sube->save()){
            session()->flash('success','Yeni şube eklendi');
            return redirect(route('subeler.index'));
        }
        else{
            session()->flash('error','Yeni şube eklenemedi.Bilgileri kontrol ediniz.');
            return redirect(route('subeler.index'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sube=Sube::find($id);
        return view('backend.Sube.Sube-Create')->with('sube',$sube);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->only('adi','durum');
        $sube = Sube::find($id);
        $sube->adi =$data['adi'];
        $sube->durum = $data['durum'];
        if ($sube->save()){

                session()->flash('success','Şube düzenlendi.');
                return redirect(route('subeler.index'));
        }
        else{
            session()->flash('error','Şube düzenlenemedi.');
            return redirect(route('subeler.index'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sube = Sube::find($id);

        if ($sube->delete()){
            session()->flash('success','Şube silindi.');
            return view('backend.Sube.Subeler');
        }
        session()->flash('error','Şube Silinemedi.');
        return view('backend.Sube.Subeler');
    }
    public function hocaindex($id){
        $sube = Sube::find($id);
        return view('backend.Sube.hocaindex')->with('sube',$sube);
    }
    public function hocacreate($id){
        $sube = Sube::find($id);
        return view('backend.Sube.hocaekle')->with('sube',$sube);
    }
    public function hocastore(Request $request){
       $data= $request->only('adi','telefon','sube_id');
       $hoca = new Hoca();
       $hoca ->adi = $data['adi'];
       $hoca ->telefon = $data['telefon'];
       $hoca ->sube_id = $data['sube_id'];
       if ($hoca->save()){
           session()->flash('success','Hoca eklendi.');
           return redirect(route('subeler.hocaindex',$data['sube_id']));
       }
        session()->flash('error','Hoca eklenemedi.');
        return redirect(route('subeler.hocaindex',$data['sube_id']));

    }

    public function hocaedit($id){
        $hoca = Hoca::find($id);
        return view ('backend.Sube.hocaekle')->with('hoca',$hoca);
    }
    public function hocaupdate(Request $request,$id){
        $data = $request->only('adi','telefon');
      $hoca= Hoca::find($id);
      $hoca->adi = $data['adi'];
      $hoca->telefon=$data['telefon'];
        if ($hoca->save()){
            session()->flash('success','Hoca düzenlendi.');
            return redirect(route('subeler.hocaindex',$hoca->sube_id));
        }
        session()->flash('error','Hoca düzenlenemedi.');
        return redirect(route('subeler.hocaindex',$hoca->sube_id));
    }
    public function hocadestroy($id){
        $hoca = Hoca::find($id);
        if ($hoca->delete()){
            session()->flash('success','Hoca silindi.');
            return redirect(route('subeler.hocaindex',$hoca->sube_id));
        }
        session()->flash('error','Hoca silinemedi.');
        return redirect(route('subeler.hocaindex',$hoca->sube_id));
    }
    public function hdatatable($id)
    {
        $data = Hoca::where('sube_id',$id)->latest()->get();
        return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function($row){
                $btn ='<div class="d-flex" >
                   <a href="#" class="delete btn btn-outline-light text-warning btn-sm" id="modal" data-action-target="/subeler/hocadestroy/'.$row->id.'" onclick="onay(this)"><i class="fa fa-trash"></i></a>
                   <a href="/subeler/'.$row->id.'/hoca/edit" class="edit btn btn-outline-light text-success btn-sm"><i class="fa fa-edit"></i></a>
                
                </div>';
                return $btn;
            })
            ->rawColumns(['action'])
            ->make(true);
    }
}
