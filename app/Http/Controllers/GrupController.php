<?php

namespace App\Http\Controllers;


use App\Durum;
use App\Grup;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class GrupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.Ayarlar.Grup.home');
    }


    public function datatable()
    {
        $data = Grup::latest()->get();
        return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function($row){
                $btn ='<div class="d-flex" >
                   <a href="#" class="edit btn btn-outline-light text-warning btn-sm" id="modal-delete" data-action-target="/ayarlar/grup/'.$row->id.'" onclick="onay(this);" "><i class="fa fa-trash"></i></a>
                   <a href="/ayarlar/grup/'.$row->id.'/edit" class="edit btn btn-outline-light text-success btn-sm" ><i class="fa fa-edit"></i></a>
                </div>';
                return $btn;
            })
            ->rawColumns(['action'])
            ->make(true);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return  view('backend.Ayarlar.Grup.ekle');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data=$request->only('adi');
        $grup = new Grup();
        $grup->adi= $request->adi;
        if ($grup->save()){
            session()->flash('success','Grup eklendi');
            return redirect(route('grup.index'));
        }
        session()->flash('error','Grup eklenemedi, bilgileri kontoller ediniz.');
        return redirect(route('grup.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Grup  $grup
     * @return \Illuminate\Http\Response
     */
    public function show(Grup $grup)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Grup  $grup
     * @return \Illuminate\Http\Response
     */
    public function edit(Grup $grup)
    {
        return view('backend.Ayarlar.Grup.ekle')->with('durum',$grup);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Grup  $grup
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Grup $grup)
    {
        $data= $request->only('adi');
        $grup->update($data);
        if ($grup->save()){
            session()->flash('success','Grup eklendi');
            return view('backend.Ayarlar.Grup.home');
        }
        session()->flash('error','Grup eklenemedi, bilgileri kontoller ediniz.');
        return view('backend.Ayarlar.Grup.home');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Grup  $grup
     * @return \Illuminate\Http\Response
     */
    public function destroy(Grup $grup)
    {
        if ($grup->delete()){
            session()->flash('success','Grup silindi.');
            return view('backend.Ayarlar.Grup.home');
        }
        session()->flash('error','Grup silinemedi.');
        return view('backend.Ayarlar.Grup.home');
    }
}
