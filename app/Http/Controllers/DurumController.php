<?php

namespace App\Http\Controllers;

use App\Durum;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class DurumController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.Ayarlar.Durum.home');

    }
    public function datatable()
    {
        $data = Durum::latest()->get();
        return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function($row){
                $btn ='<div class="d-flex" >
                   <a href="#" class="edit btn btn-outline-light text-warning btn-sm" id="modal-delete" data-action-target="/ayarlar/durum/'.$row->id.'" onclick="onay(this);" "><i class="fa fa-trash"></i></a>
                   <a href="/ayarlar/durum/'.$row->id.'/edit" class="edit btn btn-outline-light text-success btn-sm" ><i class="fa fa-edit"></i></a>
                </div>';
                return $btn;
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.Ayarlar.Durum.ekle');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data=$request->only('adi');
        $durum = new Durum();
        $durum->adi= $request->adi;
        if ($durum->save()){
            session()->flash('success','Durum eklendi');
            return redirect(route('durum.index'));
        }
        session()->flash('error','Durum eklenemedi, bilgileri kontoller ediniz.');
        return redirect(route('durum.index'));

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Durum  $durum
     * @return \Illuminate\Http\Response
     */
    public function show(Durum $durum)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Durum  $durum
     * @return \Illuminate\Http\Response
     */
    public function edit(Durum $durum)
    {
        return view('backend.Ayarlar.Durum.ekle')->with('durum',$durum);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Durum  $durum
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Durum $durum)
    {
           $data= $request->only('adi');
            $durum->update($data);
            if ($durum->save()){
                session()->flash('success','Durum eklendi');
                return view('backend.Ayarlar.Durum.home');
            }
            session()->flash('error','Durum eklenemedi, bilgileri kontoller ediniz.');
            return view('backend.Ayarlar.Durum.home');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Durum  $durum
     * @return \Illuminate\Http\Response
     */
    public function destroy(Durum $durum)
    {
        if ($durum->delete()){
            session()->flash('success','Durum silindi.');
            return view('backend.Ayarlar.Durum.home');
        }
        session()->flash('error','Durum silinemedi, bilgileri kontoller ediniz.');
        return view('backend.Ayarlar.Durum.home');
    }
}
