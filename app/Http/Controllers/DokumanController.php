<?php

namespace App\Http\Controllers;

use App\Dokuman;
use App\hasta;
use App\Temizlik;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\DataTables;

class DokumanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.Ayarlar.Dokuman.dokumanlar');
    }

    public function datatable()
    {
        $data = Dokuman::latest()->get();
        return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function($row){
                $btn ='<div class="d-flex" >
                   <a href="#" class="edit btn btn-outline-light text-warning btn-sm" id="modal-delete" data-action-target="/ayarlar/dokuman/'.$row->id.'" onclick="onay(this);" "><i class="fa fa-trash"></i></a>
                   <a href="/ayarlar/dokuman/'.$row->id.'/edit" class="edit btn btn-outline-light text-success btn-sm" ><i class="fa fa-edit"></i></a>
                </div>';
                return $btn;
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.Ayarlar.Dokuman.createDokuman');

    }
    public function download(Request $request,$id){
        $data = $request->only('dokuman_id','file');

       return Storage::download($data['file']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->only('adi');
        if (Dokuman::create($data)){
            session()->flash('success','Dokuman bilgisi eklendi');
            return redirect(route('dokuman.index'));
        }
        session()->flash('error','Dokuman bilgisi eklenemedi, bilgileri kontoller ediniz.');
        return redirect(route('dokuman.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Dokuman  $dokuman
     * @return \Illuminate\Http\Response
     */
    public function show(Dokuman $dokuman)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Dokuman  $dokuman
     * @return \Illuminate\Http\Response
     */
    public function edit(Dokuman $dokuman)
    {
        return view('backend.Ayarlar.Dokuman.createDokuman')->with('dokuman',$dokuman);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Dokuman  $dokuman
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Dokuman $dokuman)
    {
        $data= $request->only('adi');
        $dokuman->update($data);
        if ($dokuman->save()){
            session()->flash('success','Dokuman güncellendi');
            return redirect(route('dokuman.index'));
        }
        session()->flash('error','Dokuman güncellenemedi, bilgileri kontoller ediniz.');
        return redirect(route('dokuman.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Dokuman  $dokuman
     * @return \Illuminate\Http\Response
     */
    public function destroy(Dokuman $dokuman)
    {
        if ($dokuman->delete()){
            session()->flash('success','Dokuman ayarı silindi.');
            return redirect(route('dokuman.index'));
        }
        session()->flash('error','Dokuman ayarı silinemedi, bilgileri kontoller ediniz.');
        return redirect(route('dokuman.index'));
    }
}
