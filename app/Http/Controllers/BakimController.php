<?php

namespace App\Http\Controllers;

use App\Bakim;
use App\hasta;
use App\Hoca;
use App\Sube;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class BakimController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.Bakım.Tumbakim');
    }
    public function datatable()
    {
        $data = Bakim::latest()->get();
        return Datatables::of($data)
            ->addColumn('action',function ($row){
                $btn ='<div class="d-flex" >
                   <a href="#" class="delete btn btn-outline-light text-warning btn-sm" title="Veriyi sil" id="modal" data-action-target="/bakimlar/'.$row->id.'/destroy" onclick="onay(this)"><i class="fa fa-trash"></i></a>
                   <a href="/bakimlar/'.$row->id.'/edit" class="edit btn btn-outline-light text-success btn-sm" title="Düzenle"><i class="fa fa-edit"></i></a>
                </div>';
                return $btn;
            })
            ->editColumn('id', '#B{{$id}}')
            ->editColumn('hoca_id',function ($row){
                $hoca=Hoca::find($row->hoca_id);
                return $hoca->adi;
            })
            ->editColumn('sube_id',function ($row){
                $sube=Sube::find($row->sube_id);
                return $sube->adi;
            })
            ->make(true);
    }

    public function bakilanlar(){
        return view ('backend.Bakım.bakilanlar');
    }
    public function bakilandatatable()
    {
        $data = Bakim::where('bakildimi','Evet')->latest()->get();
        return Datatables::of($data)
            ->addColumn('action',function ($row){
                $btn ='<div class="d-flex" >
                   <a href="#" class="delete btn btn-outline-light text-warning btn-sm" id="modal" data-action-target="/bakimlar/'.$row->id.'/destroy" onclick="onay(this)"><i class="fa fa-trash"></i></a>
                   <a href="/bakimlar/'.$row->id.'/edit" class="edit btn btn-outline-light text-success btn-sm"><i class="fa fa-edit"></i></a>
                </div>';
                return $btn;
            })
            ->editColumn('id', '#B{{$id}}')
            ->editColumn('hoca_id',function ($row){
                $hoca=Hoca::find($row->hoca_id);
                return $hoca->adi;
            })
            ->editColumn('sube_id',function ($row){
                $sube=Sube::find($row->sube_id);
                return $sube->adi;
            })
            ->make(true);
    }

    public function bakilmayanlar(){
    return view ('backend.Bakım.bakilmayan');
}
    public function bakilmayandatatable()
    {
        $data = Bakim::where('bakildimi','Hayır')->latest()->get();
        return Datatables::of($data)
            ->addColumn('action',function ($row){
                $btn ='<div class="d-flex" >
                   <a href="#" class="delete btn btn-outline-light text-warning btn-sm" id="modal" data-action-target="/bakimlar/'.$row->id.'/destroy" onclick="onay(this)"><i class="fa fa-trash"></i></a>
                   <a href="/bakimlar/'.$row->id.'/edit" class="edit btn btn-outline-light text-success btn-sm"><i class="fa fa-edit"></i></a>
                </div>';
                return $btn;
            })
            ->editColumn('id', '#B{{$id}}')
            ->editColumn('hoca_id',function ($row){
                $hoca=Hoca::find($row->hoca_id);
                return $hoca->adi;
            })
            ->editColumn('sube_id',function ($row){
                $sube=Sube::find($row->sube_id);
                return $sube->adi;
            })
            ->make(true);
    }

    public function tedavikabul(){
    return view ('backend.Bakım.tedavikabuller');
}
    public function tedavikabuldatatable()
    {
        $data = Bakim::where('tedavikabul','Evet')->latest()->get();
        return Datatables::of($data)
            ->addColumn('action',function ($row){
                $btn ='<div class="d-flex" >
                   <a href="#" class="delete btn btn-outline-light text-warning btn-sm" id="modal" data-action-target="/bakimlar/'.$row->id.'/destroy" onclick="onay(this)"><i class="fa fa-trash"></i></a>
                   <a href="/bakimlar/'.$row->id.'/edit" class="edit btn btn-outline-light text-success btn-sm"><i class="fa fa-edit"></i></a>
                </div>';
                return $btn;
            })
            ->editColumn('id', '#B{{$id}}')
            ->editColumn('hoca_id',function ($row){
                $hoca=Hoca::find($row->hoca_id);
                return $hoca->adi;
            })
            ->editColumn('sube_id',function ($row){
                $sube=Sube::find($row->sube_id);
                return $sube->adi;
            })
            ->make(true);
    }

    public function tedavired(){
        return view ('backend.Bakım.tedavired');
    }
    public function tedavireddatatable()
    {
        $data = Bakim::where('tedavikabul','Hayır')->orWhere('tedavikabul','Düşünüyor')->latest()->get();
        return Datatables::of($data)
            ->addColumn('action',function ($row){
                $btn ='<div class="d-flex" >
                   <a href="#" class="delete btn btn-outline-light text-warning btn-sm" id="modal" data-action-target="/bakimlar/'.$row->id.'/destroy" onclick="onay(this)"><i class="fa fa-trash"></i></a>
                   <a href="/bakimlar/'.$row->id.'/edit" class="edit btn btn-outline-light text-success btn-sm"><i class="fa fa-edit"></i></a>
                </div>';
                return $btn;
            })
            ->editColumn('id', '#B{{$id}}')
            ->editColumn('hoca_id',function ($row){
                $hoca=Hoca::find($row->hoca_id);
                return $hoca->adi;
            })
            ->editColumn('sube_id',function ($row){
                $sube=Sube::find($row->sube_id);
                return $sube->adi;
            })
            ->make(true);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $subeler= Sube::all();
        return view('backend.Bakım.create')->with('subeler',$subeler);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->only('adi','sube_id','hoca_id','aciklama','bakildimi','tedavikabul','telefon','sonuc');
        $bakim = new Bakim();
        if ($bakim->fill($data)){
            $bakim->save();
            session()->flash('success','Yeni bakım eklendi');
            return redirect(route('bakimlar.index'));
        }
        else{
            session()->flash('error','Yeni bakım eklenemedi.Bilgileri kontrol ediniz.');
            return redirect(route('bakimlar.index'));
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Bakim  $bakim
     * @return \Illuminate\Http\Response
     */
    public function show(Bakim $bakim)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Bakim  $bakim
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $subeler= Sube::all();
        $bakim= Bakim::find($id);
        $hoca = Hoca::where('sube_id',$bakim->sube_id)->get();
        return view('backend.Bakım.create')->with('bakim',$bakim)->with('subeler',$subeler)->with('hocalar',$hoca);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Bakim  $bakim
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->only('adi','sube_id','hoca_id','aciklama','bakildimi','tedavikabul','telefon','sonuc');
        $bakim=Bakim::find($id);
        if ($bakim->update($data)){
            $bakim->save();
            session()->flash('success','Bakım düzenlendi');
            return redirect(route('bakimlar.index'));
        }
        session()->flash('success','Bakım düzenlenemedi');
        return redirect(route('bakimlar.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Bakim  $bakim
     * @return \Illuminate\Http\Response
     */
    public function destroy2($id)
    {
       $bakim= Bakim::find($id);
        if ($bakim->delete()){
            session()->flash('success','Bakım bilgisi silindi');
            return redirect(route('bakimlar.index'));
        }
        session()->flash('success','Bakım bilgisi silinemedi');
        return redirect(route('bakimlar.index'));
    }
}
