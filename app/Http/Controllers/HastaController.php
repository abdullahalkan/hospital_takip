<?php

namespace App\Http\Controllers;

use App\Dokuman;
use App\Durum;
use App\Grup;
use App\hasta;
use App\Hoca;
use App\Http\Requests\HastaeklemeRequest;
use App\Odeme;
use App\Okuma;
use App\Sube;
use App\Temizlik;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\DataTables;

class HastaController extends Controller
{

    public function __construct()
    {
        // Middleware only applied to these methods
        $this->middleware('isAdmin', ['only' => [
            'muhasebee' // Could add bunch of more methods too
        ]]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index()
    {
        Return view('backend.hastalar');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $subeler= Sube::all();
        $durumlar = Durum::all();
        $gruplar = Grup::all();
        return view('backend.yhasta')->with('subeler',$subeler)->with('durumlar',$durumlar)->with('gruplar',$gruplar);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(HastaeklemeRequest $request)
    {
        $connect_web = simplexml_load_file('http://www.tcmb.gov.tr/kurlar/today.xml');

        $usd_buying = $connect_web->Currency[0]->BanknoteBuying;
        $usd_selling = $connect_web->Currency[0]->BanknoteSelling;
        $euro_buying = $connect_web->Currency[3]->BanknoteBuying;
        $euro_selling = $connect_web->Currency[3]->BanknoteSelling;
        $kurlar =[
            'usd-alis' =>$usd_buying,
            'usd-satis'=>$usd_selling,
            'euro-alis' =>$euro_buying,
            'euro-satis' =>$euro_selling,
        ];
        $data=$request->only('adi','telefon','aile','sube_id','hoca_id','durum','grup','arama','aranan','yapilacaklar','ucret','alinan_ucret','deger');
        $data['user_id']=auth()->user()->id;
        $data['arandi']=false;
        $data['deger']=null;
        if ($request->para_birimi =="EURO"){
            $data['deger']=$data['ucret'];
            $data['ucret'] = $data['ucret']* $kurlar['euro-satis'];
            $data['alinan_ucret'] = $data['alinan_ucret']* $kurlar['euro-satis'];

        }
        elseif ($request->para_birimi =="DOLAR"){
            $data['deger']=$data['ucret'];
            $data['ucret'] = $data['ucret']* $kurlar['usd-satis'];
            $data['alinan_ucret'] = $data['alinan_ucret']* $kurlar['usd-satis'];

        }

            if ($hasta = hasta::create($data)){

                $hasta->odemes()->create(['tutar'=>$data['alinan_ucret'],'para_birimi' =>$request->para_birimi,'deger'=>$data['deger']]);
                session()->flash('success','Yeni hasta eklendi');
                return redirect(route('hastalar.tumhastalar'));
            }
            else{
                session()->flash('error','Yeni hasta eklenemedi.Bilgileri kontrol ediniz.');
                return redirect(route('hastalar.tumhastalar'));
            }

    }

    public function DynamicSelect(Request $request)
    {
        $data=$request->only('value');
        $sube = Sube::find($data['value']);
        $hocalar=$sube->hocas;

        return response()->json($hocalar);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function datatable()
    {
        if (!auth()->user()->role=='SuperAdmin'){
            $data = hasta::where('user_id',auth()->user()->id)->latest()->get();
        }
        else{
            $data = hasta::latest()->get();
        }

        return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function($row){
                $btn ='<div class="d-flex" >
                   <a href="#" class="delete btn btn-outline-light text-warning btn-sm" title="Hastayı Sil" id="modal" data-action-target="/hastalar/'.$row->id.'" onclick="onay(this)"><i class="fa fa-trash"></i></a>
                   <a href="/hastalar/'.$row->id.'/duzenle" title="Hastayı Düzenle" class="edit btn btn-outline-light text-success btn-sm"><i class="fa fa-edit"></i></a>
                </div>';
                return $btn;
            })
            ->addIndexColumn()
            ->addColumn('odeme', function($row){
                $durum = '<span>'.strval($row->ucret).' TL den / '.strval($row->alinan_ucret).' Tl alındı</span>';
                return $durum;
            })
            ->rawColumns(['odeme','action','id','adi'])
            ->addColumn('id', function ($row){
                $a = '<a href="hastalar/'.$row->id.'/duzenle" style="text-decoration:none"> ' . $row->id .'</a>';
                return $a;
            })
            ->addColumn('adi', function ($row){
                $a = '<a href="hastalar/'.$row->id.'/duzenle" style="text-decoration:none"> ' . $row->adi .'</a>';
                return $a;
            })
            ->editColumn('hoca_id',function ($row){
                $hoca=Hoca::find($row->hoca_id);
                return $hoca->adi;
            })
            ->make(true);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $okumalar = Okuma::all();
        $temizlikler =Temizlik::all();
        $dokumanlar = Dokuman::all();
        $subeler= Sube::all();
        $durumlar = Durum::all();
        $gruplar = Grup::all();
        $hasta = hasta::find($id);
        $hocalar = Hoca::where('sube_id',$hasta->sube_id)->get();
        return view('backend.home.edit')->with('hasta',$hasta)->with('subeler',$subeler)
            ->with('durumlar',$durumlar)->with('gruplar',$gruplar)->with('hocalar',$hocalar)
            ->with('okumalar',$okumalar)->with('temizlikler',$temizlikler)->with('dokumanlar',$dokumanlar);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data=$request->only('adi','telefon','aile','hoca_id','durum','grup','arama','aranan','yapilacaklar');
        $hasta = hasta::find($id);
        $hasta->adi = $data['adi'];
        $hasta->telefon = $data['telefon'];
        $hasta->aile = $data['aile'];
        $hasta->hoca_id = $data['hoca_id'];
        $hasta->durum = $data['durum'];
        $hasta->grup = $data['grup'];
        $hasta->arama = $data['arama'];
        $hasta->aranan = $data['aranan'];
        $hasta->yapilacaklar = $data['yapilacaklar'];
        if ($hasta->save()){
            session()->flash('success','Hasta düzenlendi');
            return redirect(route('hastalar.tumhastalar'));
        }
        else{
            session()->flash('error','Hasta düzenlenemedi.');
            return redirect(route('hastalar.tumhastalar'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $hasta = hasta::find($id);
        if ($hasta->delete()){
            session()->flash('success','Hasta silindi.');
            return view('backend.hastalar');
        }
        session()->flash('error','Hasta Silinemedi.');
        return view('backend.hastalar');
    }
    public function arama_kontrol(){
        $today = new Carbon('now');
        $tomorrow = new Carbon('now');
        $tomorrow = $tomorrow->add(1,'day');
        if (auth()->user()->role == 'SuperAdmin'){
            $hasta = hasta::where('arama','>=',$today)
                ->where('arama','<=',$tomorrow)
                ->get();
            dd($hasta);
        }
        //$date=date('d-m-Y H:i',strtotime( "30-03-2020 11:00"));

//        $hasta = hasta::whereDay('arama',[$today,$tomorrow])->get();
    }
    public function arandi($id){
        $hasta = hasta::find($id);
        if ($hasta->arandi ==true){
            $hasta->arandi=false;
            $hasta->save();
        }
        else{
            $hasta->arandi=true;
            $hasta->save();
        }
        return back();
    }
    public function odemesil($id){
        $odeme = Odeme::find($id);
        $tutar = $odeme->tutar;
        $hasta= $odeme->hasta;

        if ($odeme->delete()){
           $hasta->alinan_ucret = $hasta->alinan_ucret - ($tutar);
           $hasta->save();
            session()->flash('success','Ödeme silindi.');
            return back();
        }
        session()->flash('error','Ödeme Silinemedi.');
        return back();
    }
    public function odemeekle(Request $request){
        $connect_web = simplexml_load_file('http://www.tcmb.gov.tr/kurlar/today.xml');

        $usd_buying = $connect_web->Currency[0]->BanknoteBuying;
        $usd_selling = $connect_web->Currency[0]->BanknoteSelling;
        $euro_buying = $connect_web->Currency[3]->BanknoteBuying;
        $euro_selling = $connect_web->Currency[3]->BanknoteSelling;
        $kurlar =[
            'usd-alis' =>$usd_buying,
            'usd-satis'=>$usd_selling,
            'euro-alis' =>$euro_buying,
            'euro-satis' =>$euro_selling,
        ];
      $data= $request->only('tutar','para_birimi','deger');
        if ($request->para_birimi =="EURO"){
            $data['deger']=$data['tutar'];
            $data['tutar'] = $data['tutar']* $kurlar['euro-satis'];

        }
        elseif ($request->para_birimi =="DOLAR"){
            $data['deger']=$data['tutar'];
            $data['tutar'] = $data['tutar']* $kurlar['usd-satis'];

        }
      $hasta = hasta::find($request->id);
      if ($hasta->odemes()->create($data)){
          $toplam = $hasta->alinan_ucret+$data['tutar'];
          $hasta->alinan_ucret= $toplam;
          $hasta->save();
          session()->flash('success','Ödeme eklendi.');
          return back();
      }
        session()->flash('error','Ödeme eklenemedi.');
        return back();

    }
    public function muhasebee(){
        if (!auth()->user()->role=='SuperAdmin'){
             $gelir= Odeme::join('hastas',function ($join){
                $join->on('hasta_id','=','hastas.id')
                    ->where('user_id',auth()->user()->id);
            })
                ->get();;
        }
        else{
            $gelir = Odeme::all();
        }

        $gelirler=0;
        //burda kaldım. Gelirler giriş yapan kullanıcıya göre hessaplanacak. ansayfadaki de öyle.
        foreach ($gelir as $g){
            $gelirler += $g->tutar;
        }
        return view('backend.Muhasebe')->with('gelirler',$gelirler);
    }
    public function mdatatable()
    {
        if (!auth()->user()->role=='SuperAdmin'){

            $data = Odeme::join('hastas',function ($join){
               $join->on('hasta_id','=','hastas.id')
                   ->where('user_id',auth()->user()->id);
            })

            ->get();;
        }
        else{
            $data = Odeme::latest()->get();
        }

        return Datatables::of($data)
            ->editColumn('tutar','{{$tutar}} ₺')
            ->make(true);
    }
    public function hastaokuma(Request $request,$id){
        $data = $request->only('okuma_id','tarih');
        $hasta = hasta::find($id);

       $hasta->okumas()->attach($data['okuma_id'],['tarih' =>$data['tarih']]);
        if ($hasta){
            session()->flash('success','Okuma bilgisi eklendi.');
            return back();
        }
        session()->flash('error','Okuma eklenemedi.');
        return back();
        }
    public function hastaokumasil($id,Request $request){
        $data = $request->only('okuma_id','tarih');
            $hasta = hasta::find($id);
            if ($hasta->okumas()->where('hasta_id',$id)->wherePivot('tarih',$data['tarih'])->detach($data['okuma_id'])){
                session()->flash('success','Okuma bilgisi silindi.');
                return back();
            }
            session()->flash('error','Okuma silinemedi.');
            return back();
        }

    public function hastatemizlik(Request $request,$id){

        $data = $request->only('temizlik_id','tarih');
        $hasta = hasta::find($id);

        $hasta->temizliks()->attach($data['temizlik_id'],['tarih' =>$data['tarih']]);
        if ($hasta){
            session()->flash('success','Temizlik bilgisi eklendi.');
            return back();
        }
        session()->flash('error','Temizlik eklenemedi.');
        return back();
    }
    public function hastatemizliksil($id,Request $request){
        $data = $request->only('temizlik_id','tarih');
        $hasta = hasta::find($id);
        if ($hasta->temizliks()->where('temizlik_id',$id)->wherePivot('tarih',$data['tarih'])->detach($data['temizlik_id'])){
            session()->flash('success','Temizlik bilgisi silindi.');
            return back();
        }
        session()->flash('error','Temizlik silinemedi.');
        return back();
    }

    public function hastadokuman(Request $request,$id){
        $data = $request->only('dokuman_id','file');
        $hasta = hasta::find($id);
        $src = $request->file->store('docs/hasta/'.$id);
        $hasta->dokumans()->attach($data['dokuman_id'],['file' =>$src]);
        if ($hasta){
            session()->flash('success','Dokuman bilgisi eklendi.');
            return back();
        }
        session()->flash('error','Dokuman eklenemedi.');
        return back();
    }
    public function hastadokumansil($id,Request $request){
        $data = $request->only('dokuman_id','file');

        $hasta = hasta::find($id);
        if ($hasta->dokumans()->where('dokuman_id',$id)->wherePivot('file',$data['file'])->detach($data['dokuman_id'])){
            session()->flash('success','Döküman bilgisi silindi.');
            Storage::delete($data['file']);
            return back();
        }
        session()->flash('error','Döküman silinemedi.');
        return back();
    }
}
