<?php

namespace App\Http\Controllers;

use App\Okuma;
use App\Temizlik;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class TemizlikController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.Ayarlar.Temizlik.temizlikler');
    }
    public function datatable()
    {
        $data = Temizlik::latest()->get();
        return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function($row){
                $btn ='<div class="d-flex" >
                   <a href="#" class="edit btn btn-outline-light text-warning btn-sm" id="modal-delete" data-action-target="/ayarlar/temizlik/'.$row->id.'" onclick="onay(this);" "><i class="fa fa-trash"></i></a>
                   <a href="/ayarlar/temizlik/'.$row->id.'/edit" class="edit btn btn-outline-light text-success btn-sm" ><i class="fa fa-edit"></i></a>
                </div>';
                return $btn;
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.Ayarlar.Temizlik.temizlikCreate');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->only('adi');
        if (Temizlik::create($data)){
            session()->flash('success','Temizlik bilgisi eklendi');
            return redirect(route('temizlik.index'));
        }
        session()->flash('error','Temizlik bilgisi eklenemedi, bilgileri kontoller ediniz.');
        return redirect(route('temizlik.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Temizlik  $temizlik
     * @return \Illuminate\Http\Response
     */
    public function show(Temizlik $temizlik)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Temizlik  $temizlik
     * @return \Illuminate\Http\Response
     */
    public function edit(Temizlik $temizlik)
    {
        return view('backend.Ayarlar.Temizlik.temizlikCreate')->with('temizlik',$temizlik);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Temizlik  $temizlik
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Temizlik $temizlik)
    {
        $data= $request->only('adi');
        $temizlik->update($data);
        if ($temizlik->save()){
            session()->flash('success','Temizlik güncellendi');
            return redirect(route('temizlik.index'));
        }
        session()->flash('error','Temizlik güncellenemedi, bilgileri kontoller ediniz.');
        return redirect(route('temizlik.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Temizlik  $temizlik
     * @return \Illuminate\Http\Response
     */
    public function destroy(Temizlik $temizlik)
    {
        if ($temizlik->delete()){
            session()->flash('success','Temizlik ayarı silindi.');
            return redirect(route('temizlik.index'));
        }
        session()->flash('error','Temizlik ayarı silinemedi, bilgileri kontoller ediniz.');
        return redirect(route('temizlik.index'));
    }
}
