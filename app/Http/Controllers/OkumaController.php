<?php

namespace App\Http\Controllers;

use App\Durum;
use App\Okuma;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class OkumaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.Ayarlar.Okuma.okumalar');
    }

    public function datatable()
    {
        $data = Okuma::latest()->get();
        return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function($row){
                $btn ='<div class="d-flex" >
                   <a href="#" class="edit btn btn-outline-light text-warning btn-sm" id="modal-delete" data-action-target="/ayarlar/okuma/'.$row->id.'" onclick="onay(this);" "><i class="fa fa-trash"></i></a>
                   <a href="/ayarlar/okuma/'.$row->id.'/edit" class="edit btn btn-outline-light text-success btn-sm" ><i class="fa fa-edit"></i></a>
                </div>';
                return $btn;
            })
            ->rawColumns(['action'])
            ->make(true);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.Ayarlar.Okuma.createOkuma');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $data = $request->only('adi');
       if (Okuma::create($data)){
           session()->flash('success','Okuma eklendi');
           return redirect(route('okuma.index'));
       }
        session()->flash('error','Okuma eklenemedi, bilgileri kontoller ediniz.');
        return redirect(route('okuma.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Okuma  $okuma
     * @return \Illuminate\Http\Response
     */


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Okuma  $okuma
     * @return \Illuminate\Http\Response
     */
    public function edit(Okuma $okuma)
    {
        return view('backend.Ayarlar.Okuma.createOkuma')->with('okuma',$okuma);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Okuma  $okuma
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Okuma $okuma)
    {
        $data= $request->only('adi');
        $okuma->update($data);
        if ($okuma->save()){
            session()->flash('success','Okuma güncellendi');
            return redirect(route('okuma.index'));
        }
        session()->flash('error','Okuma güncellenemedi, bilgileri kontoller ediniz.');
         return redirect(route('okuma.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Okuma  $okuma
     * @return \Illuminate\Http\Response
     */
    public function destroy(Okuma $okuma)
    {
        if ($okuma->delete()){
            session()->flash('success','Okuma silindi.');
            return redirect(route('okuma.index'));
        }
        session()->flash('error','Okuma silinemedi, bilgileri kontoller ediniz.');
        return redirect(route('okuma.index'));
    }
}
