<?php

namespace App\Http\Controllers;

use App\hasta;
use App\Not;
use Illuminate\Http\Request;

class NotController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $not=Not::find($id);
        return view("backend.Ayarlar.not.not_goster")->with('not',$not);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$id)
    {
        $data= $request->only('text');
        $hasta = hasta::find($id);
        if ($hasta->nots()->create($data)){

            session()->flash('success','Not eklendi.');
            return back();
        }
        session()->flash('error','Not eklenemedi.');
        return back();

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Not  $not
     * @return \Illuminate\Http\Response
     */
    public function show(Not $not)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Not  $not
     * @return \Illuminate\Http\Response
     */
    public function edit(Not $not)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Not  $not
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Not $not)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Not  $not
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $not = Not::find($id);
        if ($not->delete()){

            session()->flash('success','Not silindi.');
            return back();
        }
        session()->flash('error','Not silinemedi.');
        return back();
    }
}
