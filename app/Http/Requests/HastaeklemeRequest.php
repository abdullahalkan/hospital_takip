<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class HastaeklemeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "adi" =>"required",
            "telefon" =>"required",
            "aile" =>"required",
            "sube_id" =>"required",
            "hoca_id" =>"required",
            "durum" =>"required",
            "grup" =>"required",
            "arama" =>"required",
            "yapilacaklar" =>"required | min:3",
            "ucret" =>"required | integer",
            "alinan_ucret" =>"required | integer",
        ];
    }
    public function messages()
{
    return [
        "adi.required" =>"Hasta adı ve soyadı girilmelidir.",
        "telefon.required" =>"Telefon numarası girilmelidir.",
        "aile.required" =>"Aile bilgisi girilmelidir.",
        "sube_id.required" =>"Şube seçmelisiniz.",
        "hoca_id.required" =>"Hoca seçmelisiniz.",
        "durum.required" =>"Durum bilgisi seçmelisiniz",
        "grup.required" =>"Gurup bilgisi seçmelisiniz.",
        "arama.required" =>"Arama alanını doldurmalısınız.",
        "yapilacaklar.required" =>"Yapılacaklar alanını doldurmalısınız.",
        "ucret.required" =>"Ücret alanını doldurmalısınız.",
        "alinan_ucret.required" =>"Alınan ücret alanını doldurmalısınız.",
        "ucret.integer" =>"Alınan ücret alanını sadece rakam içermelidir.",
        "alinan_ucret.integer" =>"Alınan ücret alanını sadece rakam içermelidir.",

    ];
}
}
