<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class KullnaciCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
                'name' => 'required|string|max:255',
                'email' => 'required|string | email | max:255 | unique:users',
                'password' => 'required | string | min:6 |confirmed'

        ];
    }
    public function messages()
    {
        return [
                'name.required' =>'Ad Soyad alanı gerekli.',
                'name.string' =>'İsim alanı sadece yazı karakteri içerebilir.',
                'email.required' =>'E-mail alanı gerekli',
                'email.unique' =>'Bu E-mail ile daha önce bir kayıt alınmış',
                'password.required' =>'Şifre gerekli.',
                'password.confirmed' =>'Şifreler bir birinden farklı. Kontrol ediniz.',
                'password.min' =>'Şifre en az 6 karakter olmalı'

            ];
    }
}
