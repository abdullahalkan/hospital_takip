<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dokuman extends Model
{
    protected $table ='dokumans';
    protected $fillable =['adi'];

    public function hastas(){
        return $this->belongsToMany(hasta::class)->withPivot('file')->withTimestamps();
    }
}
