<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\hasta;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;


Route::group(['prefix' => 'admin'], function() {
    Auth::routes(['register' => false]);
});

Route::group(['middleware'=>'auth'],function (){
    Route::get('/', function () {
        $subecount =\App\Sube::all()->count();
        $hocacount =\App\Hoca::all()->count();
        $hastacount =\App\hasta::all()->count();
        $dokumancount = \App\Dokuman::all()->count();
        $tutar =\App\Odeme::all();
        $para=0;
        foreach ($tutar as $t){
            $para +=$t->tutar;
        }
        $alinan =0;
        $alinanpara = \App\hasta::all();
        foreach ($alinanpara as $h){
           $alinan+= $h->alinan_ucret;
        }

        $connect_web = simplexml_load_file('http://www.tcmb.gov.tr/kurlar/today.xml');

        $usd_buying = $connect_web->Currency[0]->BanknoteBuying;
        $usd_selling = $connect_web->Currency[0]->BanknoteSelling;
        $euro_buying = $connect_web->Currency[3]->BanknoteBuying;
        $euro_selling = $connect_web->Currency[3]->BanknoteSelling;

        $kurlar =[
            'usd-alis' =>$usd_buying,
            'usd-satis'=>$usd_selling,
            'euro-alis' =>$euro_buying,
            'euro-satis' =>$euro_selling,
        ];

        $today = new Carbon('now');
        $tomorrow = new Carbon('now');
        $tomorrow = $tomorrow->add(1,'day');
       if(auth()->user()->role == 'SuperAdmin'){
           $hasta = hasta::where('arama','>=',$today)
               ->where('arama','<=',$tomorrow)
               ->orderBy('arama','asc')
               ->get();

       }else{
           $hasta = hasta::where('arama','>=',$today)
               ->where('arama','<=',$tomorrow)
               ->where('user_id',auth()->user()->id)
               ->orderBy('arama','asc')
               ->get();
       }

       //Anasayfa Aramalar listesi hastaları döndürecek
        $hastalar = hasta::where('user_id',\auth()->user()->id)->orderBy('arama','asc')->paginate(10);

        return view('backend.home.index')->with('hocacount',$hocacount)->with('hastacount',$hastacount)
            ->with('subecount',$subecount)->with('odeme',$para)->with('alinan',$alinan)->with('kurlar',$kurlar)
            ->with('dokumancount',$dokumancount)->with('hasta',$hasta)->with('hastalar',$hastalar);
    });

    Route::get('/hastalar','HastaController@index')->name('hastalar.tumhastalar');
    Route::get('/hastalar/yhasta','HastaController@create')->name('hastalar.yenihasta');
    Route::post('/hastalar/ekle','HastaController@store')->name('hastalar.ekle');
    Route::delete('/hastalar/{id}','HastaController@destroy')->name('hastalar.sil');
    Route::get('/hastalar/{id}/duzenle','HastaController@edit')->name('hastalar.edit');
    Route::put('/hastalar/{id}','HastaController@update')->name('hastalar.update');
    Route::post('/hastalar/dinamik','HastaController@DynamicSelect')->name('hastalar.dinamik');
    Route::get('/hastalar/datatable','HastaController@datatable')->name('hastalar.datatable');
    Route::get('/hastalar/arama-kontrol','HastaController@arama_kontrol')->name('hastalar.arama-kontrol');
    Route::post('/hasta/{id}/arandi','HastaController@arandi')->name('hastalar.arandi');

    Route::get('hastalar/odeme/{id}/sil','HastaController@odemesil')->name('hastalar.odemesil');
    Route::post('hastalar/odeme/ekle','HastaController@odemeekle')->name('hastalar.odemeekle');

    Route::resource('bakimlar','BakimController')->except('destroy');
    Route::get('bakimlar/datatable/get','BakimController@datatable')->name('bakimlar.datatable');
    Route::get('bakimlar/{id}/destroy','BakimController@destroy2')->name('bakimlar.destroy2');

    Route::get('bakilmlar/bakilan/bakim','BakimController@bakilanlar')->name('bakim.bakılanlar');
    Route::get('bakimlar/datatable/get/bakilanlar','BakimController@bakilandatatable')->name('bakimlar.bakilanlar.datatable');

    Route::get('bakilmlar/bakilmayan/bakim','BakimController@bakilanlar')->name('bakim.bakılmayanlar');
    Route::get('bakimlar/datatable/get/bakilmayanlar','BakimController@bakilandatatable')->name('bakimlar.bakilmayanlar.datatable');

    Route::get('bakilmlar/tedavikabul/bakim','BakimController@tedavikabul')->name('bakim.tedavikabul');
    Route::get('bakimlar/datatable/get/tedavikabul','BakimController@tedavikabuldatatable')->name('bakimlar.tedavikabul.datatable');

    Route::get('bakilmlar/tedavired/bakim','BakimController@tedavired')->name('bakim.tedavired');
    Route::get('bakimlar/datatable/get/tedavired','BakimController@tedavireddatatable')->name('bakimlar.tedavired.datatable');

    Route::get('/muhasebe','HastaController@muhasebee')->name('muhasebe.index')->middleware('isAdmin');
    Route::get('muhasebe/datatable/get','HastaController@mdatatable')->name('muhasebe.datatable')->middleware('isAdmin');

    Route::resource('/subeler','SubelerController');
    Route::get('subeler/datatable/get','SubelerController@datatable')->name('subeler.datatable');

    Route::get('subeler/{id}/hocalar','SubelerController@hocaindex')->name('subeler.hocaindex');
    Route::get('subeler/{id}/hocaekle','SubelerController@hocacreate')->name('subeler.hocacreate');
    Route::post('subeler/{id}/hocaekle','SubelerController@hocastore')->name('subeler.hocastore');
    Route::put('subeler/{id}/hocaekle','SubelerController@hocaupdate')->name('subeler.hocaupdate');
    Route::get('subeler/{id}/hoca/edit','SubelerController@hocaedit')->name('subeler.hocaedit');
    Route::get('subeler/hocadestroy/{id}','SubelerController@hocadestroy')->name('subeler.hocadelete');
    Route::get('subeler/{id}/datatable/get','SubelerController@hdatatable')->name('subeler.hdatatable');

    Route::get('/home', 'HomeController@index')->name('home');

    Route::resource('/ayarlar/durum','DurumController');
    Route::get('/ayarlar/durum/datatable/get','DurumController@datatable')->name('durum.datatable');

    Route::resource('/ayarlar/grup','GrupController');
    Route::get('/ayarlar/grup/datatable/get','GrupController@datatable')->name('grup.datatable');


    Route::get('/kullanici/{id}/delete','KullaniciController@destroy')->name('kullanici.destroy')->middleware('isAdmin');
    Route::get('/kullanici/{id}/superadmin','KullaniciController@superadmin')->name('kullanici.destroy')->middleware('isAdmin');

    Route::resource('ayarlar/okuma','OkumaController');
    Route::get('/ayarlar/okuma/datatable/get','OkumaController@datatable')->name('okuma.datatable');
    Route::post('hastalar/{id}/okuma','HastaController@hastaokuma')->name('okuma.hastaekle');
    Route::delete('hastalar/{id}/okuma','HastaController@hastaokumasil')->name('okuma.hastasil');

    Route::resource('ayarlar/temizlik','TemizlikController');
    Route::get('/ayarlar/temizlik/datatable/get','TemizlikController@datatable')->name('temizlik.datatable');
    Route::post('hastalar/{id}/temizlik','HastaController@hastatemizlik')->name('temizlik.hastaekle');
    Route::delete('hastalar/{id}/temizlik','HastaController@hastatemizliksil')->name('temizlik.hastasil');

    Route::resource('ayarlar/dokuman','DokumanController');
    Route::get('/ayarlar/dokuman/datatable/get','DokumanController@datatable')->name('dokuman.datatable');
    Route::post('hastalar/{id}/dokuman','HastaController@hastadokuman')->name('dokuman.hastaekle');
    Route::delete('hastalar/{id}/dokuman','HastaController@hastadokumansil')->name('dokuman.hastasil');
    Route::post('hasta/dokuman/{id}','DokumanController@download')->name('dokuman.download');

    Route::post('hastalar/{id}/not','NotController@store')->name('not.ekle');
    Route::delete('hastalar/{id}/not','NotController@destroy')->name('not.sil');
    Route::get("/hasta/not/{id}", "NotController@index")->name('not.goster');

});

Route::resource('kullanici','KullaniciController')->except('destroy');
Route::get('/kullanici/datatable/get','KullaniciController@datatable')->name('kullanici.datatable');
