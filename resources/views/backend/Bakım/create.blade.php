@extends('layouts.main')
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>Bakım Ekle</h2>
            </div>
            @include('layouts._alerts')
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>@isset($bakim) {{$bakim->adi}} Bakım Bilgileri @else Bakım Bilgileri @endisset  </h2>
                        </div>
                        <form action="@isset($bakim) {{route('bakimlar.update',$bakim->id)}} @else {{(route('bakimlar.store'))}} @endisset " method="post">
                            @csrf
                            @isset($bakim)
                                @method('PUT')
                                @endisset
                            <div class="form-group row ">
                                <div class="col-sm-2">
                                    <label for="sube">Şube*:</label>
                                </div>
                                <div class="col-sm-10">
                                    <select class="form-control show-tick sube" id="sube" name="sube_id" @isset($bakim) disabled @endisset  required >
                                        <option value="">Şube Seçiniz</option>
                                        @isset($subeler)
                                            @foreach($subeler as $sube)
                                                <option @isset ($bakim) @if($sube->id==$bakim->sube_id) selected
                                                        @endif @endisset value="{{$sube->id}}">{{$sube->adi}}</option>
                                            @endforeach
                                        @endisset
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-2">
                                    <label for="hoca">Hoca:</label>
                                </div>
                                <div class="col-sm-10">
                                    <select class="form-control show-tick" id="hoca" name="hoca_id">
                                        <option value="">Hoca Seçiniz</option>
                                        @isset($bakim)
                                            @isset($hocalar)
                                                @foreach($hocalar as $hoca)
                                                    @if($bakim->hoca_id == $hoca->id)
                                                        <option selected value="{{$hoca->id}}">{{$hoca->adi}}</option>
                                                    @else
                                                        <option value="{{$hoca->id}}">{{$hoca->adi}}</option>
                                                    @endif

                                                @endforeach
                                            @endisset
                                            @endisset
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-2">
                                    <label for="adisoyadi">Ad Soyad: *</label>
                                </div>
                                <div class="col-sm-10">
                                    <div class="form-line">
                                        <input type="text" class="form-control" value="@isset($bakim) {{$bakim->adi}} @endisset" name="adi" id="adisoyadi" placeholder="Hasta Adı:" minlength="4" required>
                                    </div>
                                </div>

                            </div>
                            <div class="form-group row">
                                <div class="col-sm-2">
                                    <label for="telefon">Telefon:</label>
                                </div>
                                <div class="col-sm-10">
                                    <div class="form-line">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fa fa-phone"></i></span>
                                            <input type="text" class="form-control" value="@isset($bakim){{$bakim->telefon}}@endisset" name="telefon" id="telefon" placeholder="Telefon başında 0 ile" required pattern="[0-9]{11}" maxlength="11" minlength="11">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row ">
                                <div class="col-sm-2">
                                    <label for="tedavikabul">Tedavi Kabul?</label>
                                </div>
                                <div class="col-sm-10">
                                    <select class="form-control show-tick" id="tedavikabul" name="tedavikabul" required>
                                        <option value="">Tedavi Kabul?</option>
                                            <option @isset($bakim) @if($bakim->tedavikabul =="Evet") selected  @endif @endisset value="Evet">Evet</option>
                                            <option  @isset($bakim) @if($bakim->tedavikabul =="Hayır") selected  @endif @endisset value="Hayır">Hayır</option>
                                            <option  @isset($bakim) @if($bakim->tedavikabul =="Düşünüyor") selected  @endif @endisset value="Düşünüyor">Düşünüyor</option>

                                    </select>
                                </div>
                            </div>
                            <div class="form-group row ">
                                <div class="col-sm-2">
                                    <label for="bakildimi">Bakıldı Mı?</label>
                                </div>
                                <div class="col-sm-10">
                                    <select class="form-control show-tick" id="bakildimi" name="bakildimi" required>
                                        <option  value="">Bakıldı mı?</option>
                                        <option  @isset($bakim) @if($bakim->bakildimi == "Evet") selected  @endif @endisset value="Evet">Evet</option>
                                        <option  @isset($bakim) @if($bakim->bakildimi == "Hayır") selected  @endif @endisset value="Hayır">Hayır</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-2">
                                    <label for="aciklama">Açıklama </label>
                                </div>
                                <div class="col-sm-10">
                                    <div class="form-line">
                                        <textarea type="text" class="form-control" rows="7" name="aciklama" id="aciklama" placeholder="Açıklama yazınız..." required >@isset($bakim){{$bakim->aciklama}}@endisset</textarea>
                                    </div>
                                </div>
                            </div>

                            @isset($bakim)
                                <div class="form-group row">
                                    <div class="col-sm-2">
                                        <label for="sonuc">Sonuç </label>
                                    </div>
                                    <div class="col-sm-10">
                                        <div class="form-line">
                                            <textarea type="text" class="form-control" rows="7" name="sonuc" id="sonuc" placeholder="Açıklama yazınız..." required >@isset($bakim)  {{$bakim->sonuc}}@endisset</textarea>
                                        </div>
                                    </div>
                                </div>
                            @endisset

                            <div class="form-group">
                                <div class="col-sm-12 text-center">
                                    <button type="submit" class="btn btn-raised g-bg-cyan btn-lg"> @isset($bakim) Güncelle @else Kaydet @endisset </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('css')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
    <link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/button.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
@endsection
@section('js')

    <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>

    <script>
        // Şubeye göre Hoca getirecek sorguyu post ediyoruz
        $(document).ready(function(){
            $('.sube').change(function(){
                if($(this).val() != '')
                {
                    $("#hoca option").remove(); //Var olan Hoca varsa temizler.
                    var div = $(this).parent();
                    var value = $(this).val();
                    var op = "";
                    var _token = $('input[name="_token"]').val();
                    $.ajax({
                        url:"{{ route('hastalar.dinamik') }}",
                        method:"POST",
                        data:{ value:value, _token:_token},

                        success:function(result)
                        {
                            $("#hoca").append('<option value="">Hoca Seçiniz</option>');
                            for (var i = 0; i < result.length; i++){
                                op = '<option value="'+result[i].id+'">'+result[i].adi+'</option>';
                                $("#hoca").append(op);
                            }
                        }
                    })
                }
            });
        });
    </script>
@endsection
