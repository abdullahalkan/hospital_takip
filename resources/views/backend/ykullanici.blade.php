@extends('layouts.main')
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>Yeni Kullanıcı Ekle</h2>
                <small class="text-muted">Hoşgeldiniz</small>
            </div>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2> Kullanıcı Bilgileri</h2>

                        </div>
                        <div class="body">
                            <div class="row clearfix">

                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" class="form-control" placeholder="Şube İsmi">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" class="form-control" placeholder="E-Mail">
                                    </div>
                                </div>
                            </div>
                            <div class="body"><div class="row clearfix">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="Şifre">
                                        </div>
                                    </div>
                                    <div class="body"><div class="row clearfix">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="text" class="form-control" placeholder="Şifre Tekrar">
                                                </div>
                                            </div>

                                            <div class="col-sm-3 col-xs-12">
                                                <div class="form-group drop-custum">
                                                    <select class="form-control show-tick">
                                                        <option value="">Durum</option>
                                                        <option>Aktif</option>
                                                        <option>Pasif</option>

                                                    </select>

                                                    <div class="col-sm-12">
                                                        <button type="submit" class="btn btn-raised g-bg-cyan">Kaydet</button>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
    </section>
