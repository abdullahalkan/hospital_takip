@extends('layouts.main')
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>Hasta Ekle</h2>
            </div>
    @include('layouts._alerts')
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Hasta Bilgileri </h2>
                </div>
                <form action="{{(route('hastalar.ekle'))}}" method="post">
                    @csrf
                    <div class="form-group row">
                        <div class="col-sm-2">
                            <label for="adisoyadi">Hasta Adı:</label>
                        </div>
                        <div class="col-sm-10">
                            <div class="form-line">
                                <input type="text" class="form-control " name="adi" id="adisoyadi" placeholder="Hasta Adı:" minlength="4" required>
                            </div>
                        </div>

                    </div>
                    <div class="form-group row">
                        <div class="col-sm-2">
                            <label for="telefon">Telefon:</label>
                        </div>
                        <div class="col-sm-10">
                            <div class="form-line">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-phone"></i></span>
                                    <input type="text" class="form-control" name="telefon" id="telefon" placeholder="Telefon başında 0 ile" required pattern="[0-9]{11}" maxlength="11" minlength="11">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-2">
                            <label for="aile">Aile:</label>
                        </div>
                        <div class="col-sm-10">
                            <div class="form-line">
                                <input type="text" class="form-control" name="aile" id="aile" placeholder="Aile:" required minlength="2">
                            </div>
                        </div>
                    </div>

                    <div class="form-group row ">
                        <div class="col-sm-2">
                            <label for="sube">Şube*:</label>
                        </div>
                        <div class="col-sm-10">
                            <select class="form-control show-tick sube" id="sube" name="sube_id" required >
                                <option value="">Şube Seçiniz</option>
                                @isset($subeler)
                                    @foreach($subeler as $sube)
                                        <option value="{{$sube->id}}">{{$sube->adi}}</option>
                                        @endforeach
                                    @endisset
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-2">
                            <label for="hoca">Hoca:</label>
                        </div>
                        <div class="col-sm-10">
                            <select class="form-control show-tick" id="hoca" name="hoca_id">
                                <option value="">Hoca Seçiniz</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row ">
                        <div class="col-sm-2">
                            <label for="durum">Durum:</label>
                        </div>
                        <div class="col-sm-10">
                            <select class="form-control show-tick" id="durum" name="durum" required>
                                <option value="">Durum</option>
                                @foreach($durumlar as $durum)
                                <option value="{{$durum->adi}}">{{$durum->adi}}</option>
                                    @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row ">
                        <div class="col-sm-2">
                            <label for="grup">Grup:</label>
                        </div>
                        <div class="col-sm-10">
                            <select class="form-control show-tick" id="grup" name="grup" required>
                                <option value="">Grup</option>
                                @foreach($gruplar as $grup)
                                    <option value="{{$grup->adi}}">{{$grup->adi}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-2">
                            <label for="aranan">Hastanın Arama Tarihi ve Zamanı</label>
                        </div>
                        <div class="col-sm-10">
                            <div class="form-line">
                                <input type="text" class="form-control" name="aranan" id="aranan" placeholder="Tarih ve Saat Seçiniz" required>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-2">
                            <label for="arama">Tedavi Sonrası Arama Tarihi ve Zamanı:</label>
                        </div>
                        <div class="col-sm-10">
                            <div class="form-line">
                                <input type="text" class="form-control" name="arama" id="arama" placeholder="Tarih ve Saat Seçiniz" required>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-2">
                            <label for="yapilacaklar">Yapılacaklar: </label>
                        </div>
                        <div class="col-sm-10">
                            <div class="form-line">
                                <input type="text" class="form-control" name="yapilacaklar" id="yapilacaklar" placeholder="Yapılacakalar" min="0" required>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-2">
                            <label for="ucret">Para Birimi: </label>
                        </div>
                        <div class="col-sm-10">
                            <div class="input-group">
                                <div class="form-line">
                                    <div class="input-group-prepend">
                                        <select class="form-control show-tick" id="para_birimi" name="para_birimi" required>
                                            <option value="TL" selected>TL</option>
                                            <option value="EURO">EURO</option>
                                            <option value="DOLAR">DOLAR</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-2">
                            <label for="ucret">Ücret: </label>
                        </div>
                        <div class="col-sm-10">
                            <div class="input-group">
                                <div class="form-line">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">₺</span> <input type="number" class="form-control " id="ucret" name="ucret" placeholder="Örnek : 1000" required min="0">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-2">
                            <label for="alinan_ucret">Alınan Ücret: </label>
                        </div>
                        <div class="col-sm-10">
                            <div class="input-group">
                                <div class="form-line">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">₺</span> <input type="number" class="form-control" id="alinan_ucret" name="alinan_ucret" placeholder="Örnek : 100" min="0" max="" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-12 text-center">
                            <button type="submit" class="btn btn-raised g-bg-cyan btn-lg" >Kaydet</button>
                        </div>
                    </div>
                </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @endsection
    @section('css')
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
        <link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{asset('assets/css/button.css')}}">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        @endsection
    @section('js')
        <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
        <script src="https://npmcdn.com/flatpickr/dist/flatpickr.min.js"></script>
        <script src="https://npmcdn.com/flatpickr/dist/l10n/tr.js"></script>
        <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
        <script>
            flatpickr('#arama',{
                enableTime:true,
                dateFormat:"Y-m-d H:i:ss",
                locale:"tr",
                require:true
                }
            );
            flatpickr('#aranan',{
                    enableTime:true,
                    dateFormat:"Y-m-d H:i:ss",
                    locale:"tr",
                    require:true
                }
            );
        </script>

        <script>
            // Şubeye göre Hoca getirecek sorguyu post ediyoruz
            $(document).ready(function(){

                $('.sube').change(function(){
                    if($(this).val() != '')
                    {
                        $("#hoca option").remove(); //Var olan Hoca varsa temizler.
                        var div = $(this).parent();
                        var value = $(this).val();
                        var op = "";
                        var _token = $('input[name="_token"]').val();
                        $.ajax({
                            url:"{{ route('hastalar.dinamik') }}",
                            method:"POST",
                            data:{ value:value, _token:_token},

                            success:function(result)
                            {
                                $("#hoca").append('<option value="">Hoca Seçiniz</option>');
                                for (var i = 0; i < result.length; i++){
                                    op = '<option value="'+result[i].id+'">'+result[i].adi+'</option>';
                                    $("#hoca").append(op);
                                }
                            }
                        })
                    }
                });
                $('#ucret').change(function () {
                    if($('#ucret').val() != ''){
                    var value =$('#ucret').val();
                    $('#alinan_ucret').attr({'max':value});
                    }
                })
            });
        </script>
        @endsection
