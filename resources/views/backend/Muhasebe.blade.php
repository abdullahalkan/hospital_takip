@extends('layouts.main')
@section('content')
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <section class="content">
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="card">
                        <div class="header">
                            <h2>Gelirler</h2>
                            <div class="d-flex float-right">
                                <button class="btn btn-lg btn-success text-white ml-5">KASA : <strong>{{$gelirler}} ₺</strong></button>

{{--                                <button class="btn btn-danger text-white ml-5">Gider : 0</button>--}}
                            </div>
                        </div>
                        <div class="body table-responsive mt-2">
                            <table class="table table-bordered table-striped table-hover js-basic-example dataTable" id="gelirler-table" style="width: 100%">
                                <thead>
                                <tr>
                                    <th style="width: 5%;">Tutar</th>
                                    <th style="width: 15%;">Tarih</th>
                                    <th style="width: 15%;">Para Birimi</th>
                                    <th style="width: 15%;">Döviz Değeri ( Dolar ve Euro İçin)</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>
@endsection
@section('js')


    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
    <script src="{{asset('assets/js/pages/ui/modals.js')}}"></script>
    <script src="{{asset('assets/bundles/datatablescripts.bundle.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script>
        $(document).ready(function () {
            $('#gelirler-table').DataTable({
                language:{"url":"//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Turkish.json"},
                processing: true,
                serverSide: true,
                ajax: '{!! route('muhasebe.datatable') !!}',
                columns: [
                    { data: 'tutar', name: 'tutar'},
                    { data: 'created_at', name: 'created_at' },
                    { data: 'para_birimi', name: 'para_birimi' },
                    { data: 'deger', name: 'deger' },
                ]
            });
        })
    </script>
@endsection
@section('css')
    <link href="{{asset('assets/css/button.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">

@endsection
