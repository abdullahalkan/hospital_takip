@extends('layouts.main')
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>Hasta Düzenle</h2>
            </div>
            @include('layouts._alerts')
          <div class="row card">
              <div class="row clearfix">
                 <div class="col-md-6 col-sm-12">
                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                         <div class="card">
                             <div class="header">
                                 <h2>Hasta {{$hasta->adi}}' Bilgileri </h2>
                             </div>
                             <form action="{{asset(route('hastalar.update',$hasta->id))}}" method="post">
                                 @csrf
                                 @isset($hasta)
                                     @method('PUT')
                                 @endisset
                                 <div class="form-group row">
                                     <div class="col-sm-2">
                                         <label for="adisoyadi">Hasta Adı:</label>
                                     </div>
                                     <div class="col-sm-10">
                                         <div class="form-line">
                                             <input type="text" class="form-control" value="{{$hasta->adi}}" name="adi" id="adisoyadi" placeholder="Hasta Adı:" minlength="4" required>
                                         </div>
                                     </div>

                                 </div>
                                 <div class="form-group row">
                                     <div class="col-sm-2">
                                         <label for="telefon">Telefon:</label>
                                     </div>
                                     <div class="col-sm-10">
                                         <div class="form-line">
                                             <div class="input-group-prepend">
                                                 <span class="input-group-text"><i class="fa fa-phone"></i></span>
                                                 <input type="text" class="form-control"  value="{{$hasta->telefon}}" name="telefon" id="telefon" placeholder="Telefon başında 0 ile" required pattern="[0-9]{11}" maxlength="11" minlength="11">
                                             </div>
                                         </div>
                                     </div>
                                 </div>

                                 <div class="form-group row">
                                     <div class="col-sm-2">
                                         <label for="aile">Aile:</label>
                                     </div>
                                     <div class="col-sm-10">
                                         <div class="form-line">
                                             <input type="text" class="form-control"  value="{{$hasta->aile}}" name="aile" id="aile" placeholder="Aile:" required minlength="2">
                                         </div>
                                     </div>
                                 </div>

                                 <div class="form-group row">
                                     <div class="col-sm-2">
                                         <label for="hoca">Hoca:</label>
                                     </div>
                                     <div class="col-sm-10">
                                         <select class="form-control show-tick" id="hoca" name="hoca_id">
                                             <option value="">Hoca Seçiniz</option>
                                             @foreach($hocalar as $hoca)
                                                 <option @if($hasta->hoca->id ==$hoca->id) selected @endif value="{{$hoca->id}}">{{$hoca->adi}}</option>
                                             @endforeach
                                         </select>
                                     </div>
                                 </div>
                                 <div class="form-group row ">
                                     <div class="col-sm-2">
                                         <label for="durum">Durum:</label>
                                     </div>
                                     <div class="col-sm-10">
                                         <select class="form-control show-tick" id="durum" name="durum" required>
                                             <option value="">Durum</option>
                                             @foreach($durumlar as $durum)
                                                 <option @if($hasta->durum == $durum->adi) selected @endif value="{{$durum->adi}}">{{$durum->adi}}</option>
                                             @endforeach
                                         </select>
                                     </div>
                                 </div>
                                 <div class="form-group row ">
                                     <div class="col-sm-2">
                                         <label for="grup">Grup:</label>
                                     </div>
                                     <div class="col-sm-10">
                                         <select class="form-control show-tick" id="grup" name="grup" required>
                                             <option value="">Grup</option>
                                             @foreach($gruplar as $grup)
                                                 <option @if($hasta->grup == $grup->adi) selected @endif  value="{{$grup->adi}}">{{$grup->adi}}</option>
                                             @endforeach
                                         </select>
                                     </div>
                                 </div>
                                 <div class="form-group row">
                                     <div class="col-sm-2">
                                         <label for="aranan">Hastanın Arama Tarihi ve Zamanı:</label>
                                     </div>
                                     <div class="col-sm-10">
                                         <div class="form-line">
                                             <input type="text" class="form-control" name="aranan" value="{{$hasta->aranan}}" id="aranan" placeholder="Tarih ve Saat Seçiniz" required>
                                         </div>
                                     </div>
                                 </div>
                                 <div class="form-group row">
                                     <div class="col-sm-2">
                                         <label for="arama">Tedavi Sonrası Arama Tarihi ve Zamanı:</label>
                                     </div>
                                     <div class="col-sm-10">
                                         <div class="form-line">
                                             <input type="text" class="form-control" value="{{$hasta->arama}}" name="arama" id="arama" placeholder="Tarih ve Saat Seçiniz" required>
                                         </div>
                                     </div>
                                 </div>

                                 <div class="form-group row">
                                     <div class="col-sm-2">
                                         <label for="yapilacaklar">Yapılacaklar: </label>
                                     </div>
                                     <div class="col-sm-10">
                                         <div class="form-line">
                                             <input type="text" class="form-control" value="{{$hasta->yapilacaklar}}" name="yapilacaklar" id="yapilacaklar" placeholder="Yapılacakalar" min="0" required>
                                         </div>
                                     </div>
                                 </div>


                                 <div class="form-group row">
                                     <div class="col-sm-2">
                                         <label for="ucret">Ücret: </label>
                                     </div>
                                     <div class="col-sm-10">
                                         <div class="input-group">
                                             <div class="">
                                                 <div class="input-group-prepend">
                                                     <span>{{$hasta->ucret}} TL den / {{$hasta->alinan_ucret}} alındı.</span>

{{--                                                     <span class="input-group-text">₺</span> <input type="number" value="{{$hasta->ucret}}" class="form-control " id="ucret" name="ucret" placeholder="Örnek : 1000" required min="0">--}}
                                                 </div>
                                             </div>
                                         </div>
                                     </div>
                                 </div>
                                 <div class="form-group row">
                                     <div class="col-sm-2">
                                         <label for="alinan_ucret">Ücret İşlemleri </label>
                                     </div>
                                     <div class="col-sm-10">
                                         <div class="input-group">
                                             <div class="">
                                                 <div class="input-group-prepend">
                                                     <div class="row">
                                                         <div class="col-12">
                                                             <div class="d-flex">
                                                                 <button type="button" class="btn btn-primary text-white waves-effect m-r-20" data-toggle="modal" data-target="#detayModal">Ödeme Detayları</button>
                                                                 <button type="button" class="btn btn-success text-white ml-3 waves-effect m-r-20" data-toggle="modal" data-target="#odemeModal">Yeni Ödeme Ekle</button>
                                                             </div>
                                                         </div>
                                                     </div>
                                                 </div>
                                             </div>
                                         </div>
                                     </div>
                                 </div>

                                 <div class="form-group">
                                     <div class="col-sm-12 text-center">
                                         <button type="submit" class="btn btn-raised g-bg-cyan btn-lg" title="Hastayı Güncelle">Kaydet</button>
                                     </div>
                                 </div>
                             </form>
                             <div class="modal fade" id="detayModal" tabindex="-1" role="dialog">
                                 <div class="modal-dialog" role="document">
                                     <div class="modal-content">
                                         <div class="modal-header">
                                             <h4 class="modal-title" id="defaultModalLabel">Ödeme Detayları</h4>
                                         </div>
                                         <div class="modal-body">
                                             <table class="table table-hover table-striped table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th>Ödenen Ücret</th>
                                                        <th>Ödeme Zamanı</th>
                                                        <th>İşlem</th>
                                                    </tr>
                                                </thead>
                                                 <tbody>
                                                 @foreach($hasta->odemes as $odeme)
                                                     <tr>
                                                         <td>{{$odeme->tutar}}</td>
                                                         <td>{{$odeme->created_at}}</td>
                                                         <td><a href="{{route('hastalar.odemesil',$odeme->id)}}" class="btn btn-success text-white">Sil</a></td>
                                                     </tr>
                                                     @endforeach
                                                 </tbody>
                                             </table>
                                         </div>
                                         <div class="modal-footer">
                                             <button type="button" class="btn btn-danger text-white waves-effect" data-dismiss="modal">Vazgeç</button>
                                         </div>
                                     </div>
                                 </div>
                             </div>
                             <div class="modal fade" id="odemeModal" tabindex="-1" role="dialog">
                                 <div class="modal-dialog" role="document">
                                     <div class="modal-content">
                                         <div class="modal-header">
                                             <h4 class="modal-title" id="defaultModalLabel">Ödeme Ekle</h4>
                                         </div>
                                         <form action="{{route('hastalar.odemeekle')}}" method="post">
                                         <div class="modal-body">
                                                <div class="row">
                                                    <div class="col-12">

                                                            @csrf
                                                        <div class="form-group row">
                                                            <div class="col-sm-2">
                                                                <label for="ucret">Para Birimi: </label>
                                                            </div>
                                                            <div class="col-sm-10">
                                                                <div class="input-group">
                                                                    <div class="form-line">
                                                                        <div class="input-group-prepend">
                                                                            <select class="form-control show-tick" id="para_birimi" name="para_birimi" required>
                                                                                <option value="TL" selected>TL</option>
                                                                                <option value="EURO">EURO</option>
                                                                                <option value="DOLAR">DOLAR</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                            <div class="form-group row">
                                                                <div class="col-sm-2">
                                                                    <label for="odeme">Alınan Ücret </label>
                                                                </div>
                                                                <div class="col-sm-10">
                                                                    <div class="form-line">
                                                                        <input type="number" class="form-control" id="odeme" name="tutar" placeholder="Örnek : 100" min="0" max="" required>
                                                                        <input type="hidden" class="form-control" id="odeme" value="{{$hasta->id}}" name="id" placeholder="Örnek : 100" min="0" max="" required>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                    </div>
                                                </div>
                                         </div>
                                         <div class="modal-footer">
                                             <button type="submit" class="btn btn-primary text-white waves-effect">Öde</button>
                                             <button type="button" class="btn btn-danger text-white waves-effect ml-5" data-dismiss="modal">Vazgeç</button>
                                         </div>
                                         </form>
                                     </div>
                                 </div>
                             </div>
                         </div>
                     </div>
                 </div>
                 <div class="col-md-6 col-sm-12">
                     <div class="card card-default">
                         <div class="card-header d-flex">
                             <h4 class="mt-3">Okumalar</h4>
                             <button  type="button" class="btn btn-primary ml-auto text-white" data-toggle="modal" data-target="#exampleModalLong">Okuma Bilgi Ekle</button>
                         </div>
                         <!-- Modal -->
                         <div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                             <div class="modal-dialog" role="document">
                                 <div class="modal-content">
                                     <div class="modal-header">
                                         <h5 class="modal-title" id="exampleModalLongTitle">Okuma Bilgi Ekle</h5>
                                         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                             <span aria-hidden="true">&times;</span>
                                         </button>
                                     </div>
                                     <div class="modal-body">
                                         <form action="{{route('okuma.hastaekle',$hasta->id)}}" method="post">
                                             @csrf
                                             <div class="form-group row">
                                               <div class="col-sm-3">
                                                   <label for="okuma_id">Okuma Türü</label>
                                               </div>
                                                 <div class="col-sm-9">
                                                     <select class="form-control show-tick" id="okuma_id" name="okuma_id" required>
                                                         <option value="">Okuma bilgisi seçiniz</option>
                                                         @foreach($okumalar as $okuma)
                                                             <option value="{{$okuma->id}}">{{$okuma->adi}}</option>
                                                         @endforeach
                                                     </select>
                                                 </div>
                                             </div>
                                             <div class="form-group row">
                                                 <div class="col-sm-3">
                                                     <label for="tarih">Tarih</label>
                                                 </div>
                                                 <div class="col-sm-9">
                                                     <div class="form-line">
                                                         <input type="text" class="form-control"  name="tarih" id="tarih" placeholder="Tarih ve Saat Seçiniz" required>
                                                     </div>
                                                 </div>
                                             </div>
                                             <div class="form-group row">
                                                 <div class="col-sm-12">
                                                     <div class="form-line text-center">
                                                         <button class="btn btn-success text-white ml-auto " >Ekle </button>
                                                     </div>
                                                 </div>
                                             </div>

                                         </form>
                                     </div>
                                     <div class="modal-footer">
                                         <button type="button" class="btn btn-danger  text-white" data-dismiss="modal">Vazgeç</button>
                                     </div>
                                 </div>
                             </div>
                         </div>
                         <div class="card-body">
                             <table class="table table-bordered">
                                 <thead>
                                 <tr>
                                     <th scope="col">Okuma Türü</th>
                                     <th scope="col">Okuma Zamanı</th>
                                     <th scope="col">Sil</th>
                                 </tr>
                                 </thead>
                                 <tbody>
                                @foreach($hasta->okumas as $okuma)
                                    <tr style="font-size: 11px;font-weight: normal">
                                        <td style="font-weight: normal">{{$okuma->adi}}</td>
                                        <td  style="font-weight: normal">{{$okuma->pivot->tarih}}</td>
                                        <td>
                                            <form action="{{route('okuma.hastasil',$hasta->id)}}" method="post">
                                                @csrf
                                                @method('delete')
                                                <input type="hidden" name="okuma_id" value="{{$okuma->id}}">
                                                <input type="hidden" name="tarih" value="{{$okuma->pivot->tarih}}">
                                                <button type="submit" class="btn btn-danger text-white" style="font-size: 11px;font-weight: normal"><i class="fa fa-trash" style="font-size: 15px">Sil</i> </button>
                                            </form>
                                        </td>
                                    </tr>
                                    @endforeach
                                 </tbody>
                             </table>

                         </div>
                     </div>

                     <div class="card card-default">
                         <div class="card-header d-flex">
                             <h4 class="mt-3">Temizlik</h4>
                             <button type="button" class="btn btn-primary ml-auto text-white" data-toggle="modal" data-target="#TemizlikModal">Temizlik Bilgisi Ekle</button>
                         </div>
                         <!-- Modal -->
                         <div class="modal fade" id="TemizlikModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                             <div class="modal-dialog" role="document">
                                 <div class="modal-content">
                                     <div class="modal-header">
                                         <h5 class="modal-title" id="exampleModalLongTitle">Temizlik Bilgisi Ekle</h5>
                                         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                             <span aria-hidden="true">&times;</span>
                                         </button>
                                     </div>
                                     <div class="modal-body">
                                         <form action="{{route('temizlik.hastaekle',$hasta->id)}}" method="post">
                                             @csrf
                                             <div class="form-group row">
                                               <div class="col-sm-3">
                                                   <label for="temizlik_id">Temizlik Türü</label>
                                               </div>
                                                 <div class="col-sm-9">
                                                     <select class="form-control show-tick" id="temizlik_id" name="temizlik_id" required>
                                                         <option value="">Temizlik bilgisi seçiniz</option>
                                                         @foreach($temizlikler as $temizlik)
                                                             <option value="{{$temizlik->id}}">{{$temizlik->adi}}</option>
                                                         @endforeach
                                                     </select>
                                                 </div>
                                             </div>
                                             <div class="form-group row">
                                                 <div class="col-sm-3">
                                                     <label for="temizliktarih">Tarih</label>
                                                 </div>
                                                 <div class="col-sm-9">
                                                     <div class="form-line">
                                                         <input type="text" class="form-control"  name="tarih" id="temizliktarih" placeholder="Tarih ve Saat Seçiniz" required>
                                                     </div>
                                                 </div>
                                             </div>
                                             <div class="form-group row">
                                                 <div class="col-sm-12">
                                                     <div class="form-line text-center">
                                                         <button class="btn btn-success text-white ml-auto " >Ekle </button>
                                                     </div>
                                                 </div>
                                             </div>

                                         </form>
                                     </div>
                                     <div class="modal-footer">
                                         <button type="button" class="btn btn-danger  text-white" data-dismiss="modal">Vazgeç</button>
                                     </div>
                                 </div>
                             </div>
                         </div>
                         <div class="card-body">
                             <table class="table table-bordered">
                                 <thead>
                                 <tr>
                                     <th scope="col">Temizlik Türü</th>
                                     <th scope="col">Temizlik Zamanı</th>
                                     <th scope="col">Sil</th>
                                 </tr>
                                 </thead>
                                 <tbody>
                                @foreach($hasta->temizliks as $temizlik)
                                    <tr style="font-size: 11px;font-weight: normal">
                                        <td style="font-weight: normal">{{$temizlik->adi}}</td>
                                        <td  style="font-weight: normal">{{$temizlik->pivot->tarih}}</td>
                                        <td>
                                            <form action="{{route('temizlik.hastasil',$hasta->id)}}" method="post">
                                                @csrf
                                                @method('delete')
                                                <input type="hidden" name="temizlik_id" value="{{$temizlik->id}}">
                                                <input type="hidden" name="tarih" value="{{$temizlik->pivot->tarih}}">
                                                <button type="submit" class="btn btn-danger text-white" style="font-size: 11px;font-weight: normal"><i class="fa fa-trash" style="font-size: 15px">Sil</i> </button>
                                            </form>
                                        </td>
                                    </tr>
                                    @endforeach
                                 </tbody>
                             </table>

                         </div>
                     </div>

                     <div class="card card-default">
                         <div class="card-header d-flex">
                             <h4 class="mt-3">Döküman</h4>
                             <button type="button" class="btn btn-primary ml-auto text-white" data-toggle="modal" data-target="#DokumanModal">Döküman Bilgisi Ekle</button>
                         </div>
                         <!-- Modal -->
                         <div class="modal fade" id="DokumanModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                             <div class="modal-dialog" role="document">
                                 <div class="modal-content">
                                     <div class="modal-header">
                                         <h5 class="modal-title" id="exampleModalLongTitle">Döküman Bilgisi Ekle</h5>
                                         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                             <span aria-hidden="true">&times;</span>
                                         </button>
                                     </div>
                                     <div class="modal-body">
                                         <form action="{{route('dokuman.hastaekle',$hasta->id)}}" method="post" enctype="multipart/form-data">
                                             @csrf
                                             <div class="form-group row">
                                               <div class="col-sm-3">
                                                   <label for="dokuman_id">Dokuman Türü</label>
                                               </div>
                                                 <div class="col-sm-9">
                                                     <select class="form-control show-tick" id="dokuman_id" name="dokuman_id" required>
                                                         <option value="">Dokuman bilgisi seçiniz</option>
                                                         @foreach($dokumanlar as $dokuman)
                                                             <option value="{{$dokuman->id}}">{{$dokuman->adi}}</option>
                                                         @endforeach
                                                     </select>
                                                 </div>
                                             </div>
                                             <div class="form-group row">
                                                 <div class="col-sm-3">
                                                     <label for="file"> Dosya</label>

                                                 </div>
                                                 <div class="col-sm-9">
                                                     <div class="form-line">
                                                         <div class="dz-message">
                                                             <div class="drag-icon-cph"> <i class="material-icons">touch_app</i> </div>
                                                             <h3>Yüklemek için dosya seçin ya da sürükleyin</h3>
                                                             <em>(En fazla  <strong>2mb</strong> boyutunda dosya ekleyebilirsiniz.)</em> </div>
                                                         <div class="fallback">
                                                             <input type="file"  name="file" id="file"  required>
                                                         </div>
                                                     </div>
                                                 </div>
                                             </div>
                                             <div class="form-group row">
                                                 <div class="col-sm-12">
                                                     <div class="form-line text-center">
                                                         <button class="btn btn-success text-white ml-auto " >Ekle </button>
                                                     </div>
                                                 </div>
                                             </div>

                                         </form>
                                     </div>
                                     <div class="modal-footer">
                                         <button type="button" class="btn btn-danger  text-white" data-dismiss="modal">Vazgeç</button>
                                     </div>
                                 </div>
                             </div>
                         </div>
                         <div class="card-body">
                             <table class="table table-bordered">
                                 <thead>
                                 <tr>
                                     <th scope="col" style="width: 10%">Döküman Türü</th>
                                     <th scope="col" style="width: 50%">Döküman</th>
                                     <th scope="col" style="width: 30%">Eklenme Tarihi</th>
                                     <th scope="col" style="width: 10%">Sil</th>
                                 </tr>
                                 </thead>
                                 <tbody>
                                @foreach($hasta->dokumans as $dokuman)
                                    <tr style="font-size: 11px;font-weight: normal">
                                        <td style="font-weight: normal">{{$dokuman->adi}}</td>
                                        <td  style="font-weight: normal"> <form action="{{route('dokuman.download',$dokuman->id)}}" method="post">
                                                @csrf
                                                <input type="hidden" name="dokuman_id" value="{{$dokuman->id}}">
                                                <input type="hidden" name="file" value="{{$dokuman->pivot->file}}">
                                                <button type="submit" class="btn btn-warning text-white" style="font-size: 11px;font-weight: normal"><i class="fa fa-show" style="font-size: 15px">İndir</i> </button>
                                            </form> </td>
                                        <td  style="font-weight: normal">{{$dokuman->pivot->created_at}}</td>
                                        <td>
                                            <form action="{{route('dokuman.hastasil',$hasta->id)}}" method="post">
                                                @csrf
                                                @method('delete')
                                                <input type="hidden" name="dokuman_id" value="{{$dokuman->id}}">
                                                <input type="hidden" name="file" value="{{$dokuman->pivot->file}}">
                                                <button type="submit" class="btn btn-danger text-white" style="font-size: 11px;font-weight: normal"><i class="fa fa-trash" style="font-size: 15px">Sil</i> </button>
                                            </form>
                                        </td>
                                    </tr>
                                    @endforeach
                                 </tbody>
                             </table>

                         </div>
                     </div>

                     <div class="card card-default">
                         <div class="card-header d-flex">
                             <h4 class="mt-3">Not</h4>
                             <button type="button" class="btn btn-primary ml-auto text-white" data-toggle="modal" data-target="#NotModal">Not Ekle</button>
                         </div>
                         <!-- Modal -->
                         <div class="modal fade" id="NotModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                             <div class="modal-dialog" role="document">
                                 <div class="modal-content">
                                     <div class="modal-header">
                                         <h5 class="modal-title" id="exampleModalLongTitle">Not Ekle</h5>
                                         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                             <span aria-hidden="true">&times;</span>
                                         </button>
                                     </div>
                                     <div class="modal-body">
                                         <form action="{{route('not.ekle',$hasta->id)}}" method="post" enctype="multipart/form-data">
                                             @csrf

                                             <div class="form-group row">
                                                 <div class="col-sm-3">
                                                     <label for="text"> Not :</label>

                                                 </div>
                                                 <div class="col-sm-9">
                                                     <div class="form-line">
                                                         <textarea class="form-control" id="text" name="text" rows="7"></textarea>
                                                     </div>
                                                 </div>
                                             </div>
                                             <div class="form-group row">
                                                 <div class="col-sm-12">
                                                     <div class="form-line text-center">
                                                         <button class="btn btn-success text-white ml-auto " >Ekle </button>
                                                     </div>
                                                 </div>
                                             </div>

                                         </form>
                                     </div>
                                     <div class="modal-footer">
                                         <button type="button" class="btn btn-danger  text-white" data-dismiss="modal">Vazgeç</button>
                                     </div>
                                 </div>
                             </div>
                         </div>
                         <div class="modal fade" id="NotGosterModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                             <div class="modal-dialog" role="document">
                                 <div class="modal-content">
                                     <div class="modal-header">
                                         <h5 class="modal-title" id="exampleModalLongTitle">Not Görüntüle</h5>
                                         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                             <span aria-hidden="true">&times;</span>
                                         </button>
                                     </div>
                                     <div class="modal-body">
                                             <div class="form-group row">
                                                 <div class="col-sm-3">
                                                     <label for="text"> Not :</label>

                                                 </div>
                                                 <div class="col-sm-9">
                                                     <div class="form-line">
                                                         <div class="card-body">

                                                         </div>
                                                     </div>
                                                 </div>
                                             </div>
                                     </div>
                                     <div class="modal-footer">
                                         <button type="button" class="btn btn-danger  text-white" data-dismiss="modal">Kapat</button>
                                     </div>
                                 </div>
                             </div>
                         </div>
                         <div class="card-body">
                             <table class="table table-bordered">
                                 <thead>
                                 <tr>
                                     <th scope="col" style="width: 20%">Ekleyen</th>
                                     <th scope="col" style="width: 50%">Eklenme Zamanı</th>
                                     <th scope="col" style="width: 30%">Sil</th>
                                 </tr>
                                 </thead>
                                 <tbody>
                                 @foreach($hasta->nots as $not)
                                     <tr style="font-size: 11px;font-weight: normal">
                                         <td style="font-weight: normal">{{$not->hasta->hoca->adi}}</td>
                                         <td  style="font-weight: normal">{{$not->created_at}}</td>
                                         <td>
                                             <div class="d-flex">
                                                 <form action="{{route('not.sil',$not->id)}}" method="post">
                                                     @csrf
                                                     @method('delete')
                                                     <button type="submit" class="btn btn-danger text-white" style="font-size: 11px;font-weight: normal"><i class="fa fa-trash" style="font-size: 15px">Sil</i> </button>
                                                 </form>
                                                 <a href="{{route("not.goster",$not->id)}}" title="Notu Görüntüle" class="fa fa-edit btn btn-primary text-white ml-auto" ></a>
                                             </div>
                                         </td>
                                     </tr>
                                 @endforeach
                                 </tbody>
                             </table>

                         </div>
                     </div>
                 </div>
              </div>
          </div>
        </div>
    </section>
@endsection
@section('css')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
    <link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/button.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
@endsection
@section('js')
    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
    <script src="https://npmcdn.com/flatpickr/dist/flatpickr.min.js"></script>
    <script src="https://npmcdn.com/flatpickr/dist/l10n/tr.js"></script>
    <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
    <script>
        flatpickr('#arama',{
                enableTime:true,
                dateFormat:"Y-m-d H:i:ss",

                locale:"tr",
                require:true
            }
        );flatpickr('#aranan',{
                enableTime:true,
                dateFormat:"Y-m-d H:i:ss",

                locale:"tr",
                require:true
            }
        );
        flatpickr('#tarih',{
                enableTime:true,
                dateFormat:"d-m-Y H:i",

                locale:"tr",
                require:true
            }
        );
        flatpickr('#temizliktarih',{
                enableTime:true,
                dateFormat:"d-m-Y H:i",

                locale:"tr",
                require:true
            }
        );
    </script>

@endsection
