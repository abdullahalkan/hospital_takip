<!-- #Start - Content Alanı -->
@extends('layouts.main')
@section('content')
    <section class="content home">
        <div class="container-fluid">
            <div class="block-header ">
                <h1>Anasayfa</h1>
                <a href="{{route('hastalar.yenihasta')}}" class="btn btn-primary text-white float-right " style="margin-top: -15px">Hasta Ekle</a>
                <small class="text-muted">Hoş geldiniz</small>

            </div>
            @include('layouts._alerts')
            {{--            İstatistikler --}}
            <div class="row clearfix">
                <div class="col-md-6 col-lg-3 col-sm-12">
                    <div class="card bg-warning ">
                        <div class="text-white body align-items-center text-center my-5 font-weight-bold">
                            <a href="{{route('subeler.index')}}" style="text-decoration: none" class="text-white">
                                <h1>{{$subecount}}</h1>
                                 <p class="">Toplam Şube</p>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3 col-sm-12">
                    <div class="card bg-info">
                        <div class="text-white body align-items-center text-center my-5 font-weight-bold">
                            <h1 title="Tüm şubelerdeki hoca sayısını göstermektedir.">{{$hocacount}}</h1>
                            <p class="">Toplam Hoca</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3 col-sm-12">
                    <div class="card bg-faded">
                        <div  class=" body align-items-center text-center my-5 font-weight-bold">
                            <a href="{{route('hastalar.tumhastalar')}}" style="text-decoration: none" class="text-dark" title="Tüm Hastalar">
                                <h1>{{$hastacount}}</h1>
                                <p class="">Toplam Hasta</p>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3 col-sm-12">
                    <div class="card bg-success">
                        <div class="text-white body align-items-center text-center my-5 font-weight-bold">
                            <a href="{{route('dokuman.index')}}" style="text-decoration: none" class="text-white">
                                <h1>{{$dokumancount}}</h1>
                                <p class="">Toplam Döküman</p>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            {{--Aramalar listesi--}}
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="card">
                        <div class="header">
                            <h2> Aramalar <small>Araması yaklaşan ve geçmiş aramaları listeler</small> </h2>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>#Hasta ID</th>
                                        <th>Hasta Adı</th>
                                        <th>Arama Tarihi</th>
                                        <th>Hoca</th>
                                        <th>İşlem</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                   @foreach($hastalar as $h)
                                       <tr class="@if(!$h->arandi)table-danger @else  @endif">
                                           <td><a href="{{route('hastalar.edit',$h->id)}}">#{{$h->id}}</a></td>
                                           <td><a href="{{route('hastalar.edit',$h->id)}}">{{$h->adi}} </a></td>
                                           <td>{{date('d-m-Y H:i',strtotime($h->arama)) }}</td>
                                           <td>{{$h->hoca->adi}}</td>
                                           <td><span class="label label-primary">
                                                   <a class="favorite mt-2" title="Arandı / Aranmadı"
                                                         onclick="event.preventDefault(); document.getElementById('arama-{{$h->id}}').submit();">
                                                       <i class="fa fa-phone fa-2x"></i>
                                                    </a>
                                               </span>
                                           </td>
                                       </tr>
                                       <form id="arama-{{$h->id}}" method="post" action="/hasta/{{$h->id}}/arandi">
                                           @csrf
                                       </form>
                                    @endforeach
                                    </tbody>
                                </table>
                                <div class="text-center">
                                    {{$hastalar->links()}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            {{--Ödeme İstatistikleri--}}
            @if(auth()->user()->role == "SuperAdmin")
            <div class="row clearfix">
                <div class="col-md-6 col-lg-3 col-sm-12">
                    <div class="card">
                        <div class="body">
                            <h3 class=" text-left">Toplam Ödeme</h3>
                            <div class="text-right">
                                <div class="demo-google-material-icon"> <i class="material-icons md-36 text-success">arrow_upward</i> <span class="text-muted">Toplam Ödeme</span></div>
                                <h1>
                                   {{$odeme}}
                                </h1>
                            </div>
                            <div class="progressbar-xs progress-rounded progress-striped progress ng-isolate-scope" value="68" type="success">
                                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="68" aria-valuemin="0" aria-valuemax="100" style="width: 68%;"></div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3 col-sm-12">
                    <div class="card">
                        <div class="body">
                            <h3 class=" text-left">Alınan Ödeme</h3>
                            <div class="text-right">
                                <div class="demo-google-material-icon"> <i class="material-icons md-36 text-warning">arrow_upward</i> <span class="text-muted">Toplam Ödeme</span></div>
                                <h1>
                                   {{$alinan}}
                                </h1>
                            </div>
                            <div class="progressbar-xs progress-rounded progress-striped progress ng-isolate-scope" value="68" type="danger">
                                <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="68" aria-valuemin="0" aria-valuemax="100" style="width: 68%;"></div>
                            </div>
                            </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3 col-sm-12">
                    <div class="card">
                        <div class="body">
                            <h3 class="text-left">Alacak</h3>
                            <div class="text-right">
                                <div class="demo-google-material-icon"> <i class="material-icons md-36 text-info">arrow_upward</i> <span class="text-muted">Toplam Alacak</span></div>
                                <h1>
                                    {{$odeme - $alinan}}
                                </h1>
                            </div>
                            <div class="progressbar-xs progress-rounded progress-striped progress ng-isolate-scope" value="68" type="warning">
                                <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="68" aria-valuemin="0" aria-valuemax="100" style="width: 68%;"></div>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
            @endif


            {{--        Döviz Bilgileri--}}
            <div class="row clearfix">
                <div class="col-md-6 col-lg-3 col-sm-12">
                    <div class="card">
                        <div class="body">
                            <h3 class="text-center "><i class="fa fa-usd text-danger" style="font-size: 40px"></i> </h3>
                            <h3 class="text-center ">Dolar Alış </h3>
                            <div class="text-right">
                                <h1 class="text-center">
                                    {{$kurlar['usd-alis']}}
                                </h1>
                            </div>
                            <div class="progressbar-xs progress-rounded progress-striped progress ng-isolate-scope" value="100" type="success">
                                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="68" aria-valuemin="0" aria-valuemax="100" style="width: 100%;"></div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3 col-sm-12">
                    <div class="card">
                        <div class="body">
                            <h3 class="text-center "><i class="fa fa-usd text-success" style="font-size: 40px"></i> </h3>
                            <h3 class="text-center ">Dolar Satış </h3>
                            <div class="text-right">
                                <h1 class="text-center">
                                    {{$kurlar['usd-satis']}}
                                </h1>
                            </div>
                            <div class="progressbar-xs progress-rounded progress-striped progress ng-isolate-scope" value="100" type="success">
                                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="68" aria-valuemin="0" aria-valuemax="100" style="width: 100%;"></div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3 col-sm-12">
                    <div class="card">
                        <div class="body">
                            <h3 class="text-center "><i class="fa fa-euro text-danger" style="font-size: 40px"></i> </h3>
                            <h3 class="text-center ">Euro Alış </h3>
                            <div class="text-right">
                                <h1 class="text-center">
                                    {{$kurlar['euro-alis']}}
                                </h1>
                            </div>
                            <div class="progressbar-xs progress-rounded progress-striped progress ng-isolate-scope" value="100" type="success">
                                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="68" aria-valuemin="0" aria-valuemax="100" style="width: 100%;"></div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3 col-sm-12">
                    <div class="card">
                        <div class="body">
                            <h3 class="text-center "><i class="fa fa-euro text-success" style="font-size: 40px"></i> </h3>
                            <h3 class="text-center ">Euro Satış </h3>
                            <div class="text-right">
                                <h1 class="text-center">
                                    {{$kurlar['euro-satis']}}
                                </h1>
                            </div>
                            <div class="progressbar-xs progress-rounded progress-striped progress ng-isolate-scope" value="100" type="success">
                                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="68" aria-valuemin="0" aria-valuemax="100" style="width: 100%;"></div>
                            </div>

                        </div>
                    </div>
                </div>


            </div>
        </div>
{{--        // Modal yakalaşan aramalalar--}}
        <div class="modal fade" id="alarm" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="defaultModalLabel">24 Saat İçinde Yapılması Gereken Aramalar</h4>
                    </div>
                    <div class="modal-body">
                        <div class="table-responsive">
                            @isset($hasta)
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Hasta Adı</th>
                                            <th>Arama Zamanı</th>
                                            <th>Kalan Süre</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($hasta as $h)
                                    <tr>
                                        <td style="font-size: 17px">{{$h->id}}</td>
                                        <td style="font-size: 17px"><a href="hastalar/{{$h->id}}/duzenle">{{$h->adi}}</a></td>
                                        <td style="font-size: 17px">{{date('d-m-y H:i',strtotime($h->arama))}}</td>
                                        <td><span style="font-size: 17px" class="label label-danger">{{gmdate('H:i:s',\Carbon\Carbon::now()->diffInSeconds($h->arama,false))}}</span> </td>
                                    </tr>
                                    @endforeach
                                    </tbody>

                                </table>
                                @else
                                <span>Arama Bildirimi Yok</span>
                                @endisset
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button style="font-size: 15px" type="button" class="btn btn-warning  text-white" data-dismiss="modal">Kapat</button>
                    </div>
                </div>
            </div>
        </div>

    </section>
    @endsection
<!--#End - Content Alanı-->
    @section('css')
        <link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        @endsection
    @section('js')
        <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
        <script src="{{asset('assets/js/pages/index.js')}}"></script>
        <script src="{{asset('assets/js/pages/charts/sparkline.min.js')}}"></script>
        <script type="text/javascript">
            $(window).on('load',function () {
                $('#alarm').modal('show');
            })
        </script>
        @endsection
