@extends('layouts.main')
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>@isset($hoca) Hocayı Düzenle @else Hoca Ekle @endisset</h2>
            </div>
            @include('layouts._alerts')
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>@isset($sube) {{$sube->adi}} Şubesine Hoca Ekle @endisset  </h2>
                        </div>
                        <form action="@isset($hoca) {{route('subeler.hocaupdate',$hoca->id)}} @else {{(route('subeler.hocastore',$sube->id))}} @endisset " method="post">
                            @csrf
                            @isset($hoca)
                                @method('PUT')
                            @endisset
                            <input type="hidden" value="@isset($hoca) {{$hoca->sube->id}} @else {{$sube->id}}  @endisset " name="sube_id">
                            <div class="form-group row">
                                <div class="col-sm-2">
                                    <label for="adi">Hoca Adı: *</label>
                                </div>
                                <div class="col-sm-10">
                                    <div class="form-line">
                                        <input type="text" value="@isset($hoca) {{$hoca->adi}}  @endisset" class="form-control " name="adi" id="adi" placeholder="Hoca Adı:" minlength="2" required>
                                    </div>
                                </div>

                            </div>
                            <div class="form-group row">
                                <div class="col-sm-2">
                                    <label for="telefon">Hoca Telefon: *</label>
                                </div>
                                <div class="col-sm-10">
                                    <div class="form-line">
                                        <input type="text" value="@isset($hoca) {{$hoca->telefon}}  @endisset" class="form-control" name="telefon" id="telefon" placeholder="Telefon başında 0 ile" required pattern="[0-9]{11}" maxlength="11" minlength="11">
                                    </div>
                                </div>

                            </div>

                            <div class="form-group">
                                <div class="col-sm-12 text-center">
                                    <button type="submit" class="btn btn-raised g-bg-cyan btn-lg" >@isset($hoca) Güncelle  @else Ekle @endisset</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('css')

    <link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/button.css')}}">
@endsection
@section('js')
    <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>

@endsection
