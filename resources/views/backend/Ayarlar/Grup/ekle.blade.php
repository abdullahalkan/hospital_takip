@extends('layouts.main')
@section('content')
    <section class="content">
        <div class="container-fluid">
            @include('layouts._alerts')
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>Grup Bilgileri </h2>
                        </div>
                        <form action="@isset($grup) {{asset(route('grup.update',$grup->id))}} @else {{asset(route('grup.store'))}} @endisset " method="post">
                            @csrf
                            @isset($grup)
                                @method('PUT')
                                @endisset
                            <div class="form-group row">
                                <div class="col-sm-2">
                                    <label for="adi">Grup Adı: *</label>
                                </div>
                                <div class="col-sm-10">
                                    <div class="form-line">
                                        <input type="text" value="@isset($grup) {{$grup->adi}} @endisset" class="form-control " name="adi" id="adi" placeholder="Grup Adı" minlength="1" required>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-12 text-center">
                                    <button type="submit" class="btn btn-raised g-bg-cyan btn-lg" >@isset($grup) Güncelle  @endisset Kaydet</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @endsection
@section('css')

    @endsection
@section('js')

    @endsection
