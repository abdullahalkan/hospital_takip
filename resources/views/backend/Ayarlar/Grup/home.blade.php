@extends('layouts.main')
@section('content')
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <section class="content">
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="card">
                        <div class="header">
                            <h2>Grup Ayarları</h2>
                            <ul class="header-dropdown">
                                <a href="{{route('grup.create')}}" class="yesilbutton btn-round">Grup Ekle</a>
                            </ul>
                        </div>
                        @include('layouts._alerts')
                        <div class="body table-responsive mt-2 align-items-center">
                            <table class="table table-bordered table-striped table-hover js-basic-example dataTable text-center" id="durum-table" style="width: 100%">
                                <thead>
                                <tr>
                                    <th style="width: 5%;">İD</th>
                                    <th style="width: 15%;">Adı</th>
                                    <th style="width: 10%;">İşlem</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>İD</th>
                                    <th>Adı</th>
                                    <th>İşlem</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>
@endsection
@section('css')
    <link href="{{asset('assets/css/button.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
@endsection
@section('js')
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
    <script src="{{asset('assets/bundles/datatablescripts.bundle.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script>
        $(document).ready(function () {

            $('#durum-table').DataTable({
                language:{"url":"//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Turkish.json"},
                processing: true,
                serverSide: true,
                ajax: '{!! route('grup.datatable') !!}',
                columns: [
                    { data: 'id', name: 'id'},
                    { data: 'adi', name: 'adi' },
                    { data: 'action', name: 'action', orderable: false, searchable: false }
                ]
            });
        })
    </script>
    <script>
        function onay(obj) {
            var target= obj.getAttribute('data-action-target') ;
            Swal.fire({
                title: 'Emin misiniz?',
                text: "Bunu silmek istediğinize emin misiniz? ",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                cancelButtonText: 'İptal',
                confirmButtonText: 'Sil'
            }) .then((result)=>{
                    if(result.value){
                        $.ajax({
                            type: "DELETE",
                            url: target,
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            contentType: false,
                            processData: false,
                            success: function (data) {
                                console.log(data);
                                Swal.fire({
                                        title: 'Başarılı',
                                        text: 'Grup bilgisi silindi',
                                        icon: 'success'
                                    }
                                ).then(()=>
                                    window.location.reload()
                                );

                            }
                            //silme işlemi başka sayfada olacaksa dosya adı gir
                        });
                    }
                }
            )
        }
    </script>
@endsection
