@extends('layouts.main')
@section('content')
<section class="content">
    <div class="container" >
        <div class="row justify-content-center" style="margin-top: 150px">
            <div class="col-md-10 col-sm-12">
                <div class="card ">
                    <div class="card-header bg-amber">
                        <h3>{{$not->hasta->adi}} - Hasta Notu</h3>
                    </div>
                    <div class="card-body">
                        <textarea class="form-control" name="" id="" cols="30" rows="10" disabled>{{$not->text}}</textarea>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('js')
@endsection

@section('css')
@endsection
