@extends('layouts.main')
@section('content')
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <section class="content">
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="card">
                        <div class="header">
                            <h2>Tüm Dokuman Ayarları</h2>
                            <ul class="header-dropdown">
                                <a href="{{route('dokuman.create')}}" class="yesilbutton btn-round">Yeni Dokuman Ekle</a>
                            </ul>
                        </div>
                        <div class="body table-responsive mt-2">
                            <table class="table table-bordered table-striped table-hover js-basic-example dataTable" id="dokuman-table" style="width: 100%">
                                <thead>
                                <tr>
                                    <th style="width: 5%;">ID</th>
                                    <th style="width: 15%;">Ad</th>
                                    <th style="width: 10%;">İşlemler</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>
@endsection
@section('js')
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
    <script src="{{asset('assets/js/pages/ui/modals.js')}}"></script>
    <script src="{{asset('assets/bundles/datatablescripts.bundle.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script>
        $(document).ready(function () {
            $('#dokuman-table').DataTable({
                language:{"url":"//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Turkish.json"},
                processing: true,
                serverSide: true,
                ajax: '{!! route('dokuman.datatable') !!}',
                columns: [
                    { data: 'id', name: 'id'},
                    { data: 'adi', name: 'adi' },
                    { data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });
        })
    </script>
    <script>
        function onay(obj) {
            var target= obj.getAttribute('data-action-target') ;
            console.log(target);
            Swal.fire({
                title: 'Emin misiniz?',
                text: "Bu şubeyi silmek istediğinize emin misiniz? ",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                cancelButtonText: 'İptal',
                confirmButtonText: 'Sil'
            }) .then((result)=>{
                    if(result.value){
                        $.ajax({
                            type: "DELETE",
                            url: target,
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            contentType: false,
                            processData: false,
                            success: function (data) {
                                console.log(data);
                                Swal.fire({
                                        title: 'Başarılı',
                                        text: 'Okuma silindi',
                                        icon: 'success'
                                    }
                                ).then(()=>
                                    window.location.reload()
                                );

                            }
                            //silme işlemi başka sayfada olacaksa dosya adı gir
                        });
                    }
                }
            )
        }
    </script>
@endsection
@section('css')
    <link href="{{asset('assets/css/button.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">

@endsection
