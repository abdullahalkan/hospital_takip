@extends('layouts.main')
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>@isset($okuma) Okumayı Düzenle @else Okuma Ekle @endisset</h2>
            </div>
            @include('layouts._alerts')
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>  Okuma Bilgileri </h2>
                        </div>
                        <form action="@isset($okuma) {{route('okuma.update',$okuma->id)}} @else {{(route('okuma.store'))}} @endisset " method="post">
                            @csrf
                            @isset($okuma)
                                @method('PUT')
                            @endisset
                            <div class="form-group row">
                                <div class="col-sm-2">
                                    <label for="adi"> Adı: *</label>
                                </div>
                                <div class="col-sm-10">
                                    <div class="form-line">
                                        <input type="text" value="@isset($okuma){{$okuma->adi}}@endisset" class="form-control " name="adi" id="adi" placeholder=" Adı:" minlength="2" required>
                                    </div>
                                </div>

                            </div>

                            <div class="form-group">
                                <div class="col-sm-12 text-center">
                                    <button type="submit" class="btn btn-raised g-bg-cyan btn-lg" > Ekle</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('css')

    <link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/button.css')}}">
@endsection
@section('js')
    <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>

@endsection
