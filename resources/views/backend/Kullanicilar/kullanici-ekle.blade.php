@extends('layouts.main')
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2> @isset($kullanici) {{$kullanici->user->name}} - Kullanıcı Düzenle @else Kullanıcı Ekle @endisset </h2>
            </div>
            @include('layouts._alerts')
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>Kullanıcı Bilgileri </h2>
                        </div>
                        <form action=" @isset($kullanici) {{(route('kullanici.update',$kullanici->id))}} @else {{(route('kullanici.store'))}} @endisset " method="post">
                            @csrf
                            @isset($kullanici)
                                @method('PUT')
                            @endisset
                            <div class="form-group row">
                                <div class="col-sm-2">
                                    <label for="adisoyadi">Ad Soyad:*</label>
                                </div>
                                <div class="col-sm-10">
                                    <div class="form-line">
                                        <input type="text" class="form-control " name="name" id="adisoyadi" placeholder="Ad Soyad:" value="@isset($kullanici) {{$kullanici->user->name}} @else {{ old('name') }} @endisset " minlength="4" required>
                                    </div>
                                </div>

                            </div>
                            <div class="form-group row">
                                <div class="col-sm-2">
                                    <label for="telefon">E-Mail :*</label>
                                </div>
                                <div class="col-sm-10">
                                    <div class="form-line">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fa fa-envelope"></i></span>
                                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="@isset($kullanici) {{$kullanici->user->email}} @else {{ old('email') }} @endisset " required autocomplete="email">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-2">
                                    <label for="aile">Şifre:*</label>
                                </div>
                                <div class="col-sm-10">
                                    <div class="form-line">
                                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" placeholder="Şifre" name="password" required autocomplete="new-password">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-2">
                                    <label for="yapilacaklar">Şifre Tekrar </label>
                                </div>
                                <div class="col-sm-10">
                                    <div class="form-line">
                                        <input id="password-confirm" type="password" placeholder="Şifre terkar" class="form-control" name="password_confirmation" required autocomplete="new-password">
                                    </div>
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-sm-12 text-center">
                                    <button type="submit" class="btn btn-raised g-bg-cyan btn-lg" >Kaydet</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('css')
    <link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/button.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
@endsection
@section('js')

    <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>

@endsection

