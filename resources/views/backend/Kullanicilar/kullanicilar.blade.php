@extends('layouts.main')
@section('content')
    <section class="content">
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <div class="container-fluid">
            @include('layouts._alerts')
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="card">
                        <div class="header">
                            <h2>Kullanıcılar </h2>
                            <ul class="header-dropdown">
                                <a href="{{route('kullanici.create')}}" class="yesilbutton btn-round">Yeni Kullanıcı Ekle</a>
                            </ul>
                        </div>
                        <div class="body table-responsive mt-2">
                            <table class="table table-bordered table-striped table-hover js-basic-example dataTable" id="kullanici-table">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Mail</th>
                                    <th>Ad Soyad</th>
                                    <th>Son Giriş</th>
                                    <th>Oluşturulma Tarihi</th>
                                    <th>İşlemler</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('js')
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
    <script src="{{asset('assets/js/pages/ui/modals.js')}}"></script>
    <script src="{{asset('assets/bundles/datatablescripts.bundle.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script>
        $(document).ready(function () {
            @if(session()->has('success'))
            Toast.fire({
                icon: 'success',
                title: 'Kayıt Başarılı'
            });
            @endif
            $('#kullanici-table').DataTable({
                language:{"url":"//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Turkish.json"},
                processing: true,
                serverSide: true,
                ajax: '{!! route('kullanici.datatable') !!}',
                columns: [
                    { data: 'id', name: 'id'},
                    { data: 'email', name: 'email' },
                    { data: 'name', name: 'name' },
                    { data: 'lastActive', name: 'lastActive' },
                    { data: 'created_at', name: 'created_at' },
                    { data: 'action', name:'action',searchable:false,orderable: false},
                ]
            });
        })
    </script>
    <script>
        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000,
            timerProgressBar: true,
            onOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer);
                toast.addEventListener('mouseleave', Swal.resumeTimer);
            }
        });

    </script>
    <script>
        function onay(obj) {
            var target= obj.getAttribute('data-action-target') ;
            console.log(target);
            Swal.fire({
                title: 'Emin misiniz?',
                text: "Bu bakımı silmek istediğinize emin misiniz? ",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                cancelButtonText: 'İptal',
                confirmButtonText: 'Sil'
            }) .then((result)=>{
                    if(result.value){
                        $.ajax({
                            type: "get",
                            url: target,
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            contentType: false,
                            processData: false,
                            success: function (data) {
                                console.log(data);
                                Swal.fire({
                                        title: 'Başarılı',
                                        text: 'Kullanıcı silindi',
                                        icon: 'success'
                                    }
                                ).then(()=>
                                    window.location.reload()
                                );

                            }
                            //silme işlemi başka sayfada olacaksa dosya adı gir
                        });
                    }
                }
            )
        }
    </script>
@endsection
@section('css')
    <link href="{{asset('assets/plugins/jquery-datatable/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/button.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">

@endsection
