<div class="container">
    @if($errors->any())
        <div class="alert alert-danger" style="background-color: #ff9600 !important;">
            @foreach($errors->all() as $messages)
                <li style="list-style-type: none">{{$messages}}</li>
            @endforeach
        </div>
    @endif
    @if(session()->has('success'))
        <div class="alert alert-success" style="background-color: #00b0e4 !important;">
            <strong>Başarılı!  </strong>  {{session()->get('success')}}
        </div>
    @endif
    @if(session()->has('error'))
        <div class="alert alert-warning" style="background-color: #ff9600 !important;">
            <strong>Başarısız!  </strong>    {{session()->get('error')}}
        </div>
    @endif
</div>
