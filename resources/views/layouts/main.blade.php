<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Kainat Enerjisi </title>
    <link rel="icon" href="{{asset('assets/images/Logoo/hatemhocalogo.png')}}" type="image/x-icon">
    <link href="{{asset('assets/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/plugins/morrisjs/morris.css')}}" rel="stylesheet" />
    <!-- Custom Css -->
    <link href="{{asset('assets/css/main.css')}}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins&display=swap" rel="stylesheet">
    <!-- Swift Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="{{asset('assets/css/themes/all-themes.css')}}" rel="stylesheet" />

    <style>
        .li a {
            font-family: 'Poppins', sans-serif; !important;
            font-size: 14px; !important;
            color: #2b2b2b;
        }
        li a:hover{
            color:#337ab7;
        }
        body {
            font-family: 'Poppins', sans-serif; !important;
            font-size: 14px; !important;
        }
    </style>
    @yield('css')
</head>

<body class="theme-cyan">
<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="preloader">
            <div class="spinner-layer pl-cyan">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div>
                <div class="circle-clipper right">
                    <div class="circle"></div>
                </div>
            </div>
        </div>
        <p>Yükleniyor</p>
    </div>
</div>
<!-- #END# Page Loader -->
<!-- Overlay For Sidebars -->
<div class="overlay"></div>
<!-- #END# Overlay For Sidebars -->


<!-- Top Bar -->
<nav class="navbar clearHeader">
    <div class="col-12">

        <div class="logo" style=" position:fixed;   ">
            <a class="navbar-brand" style="font-size: 18px;color: white; font-weight: bold;display: inline;text-decoration: none;margin-left: -7px" href="/">
                <div class="navbar-header"> <a href="javascript:void(0);" class="bars"></a> </div>
                <a href="/"> <img style="margin-top: -10px;margin-left: -25px" width="40px" height="40px" class="hidden-lg-down" src="{{asset('assets/images/Logoo/hatemhocalogo.png')}}" alt="hatemhoca"></a>
                <h3 class="hidden-lg-down text-white" style="display: inline">Kainat Enerjisi</h3>
            </a>
        </div>
    </div>
</nav>
<!-- #Top Bar -->
<section>
    <!-- Left Sidebar -->
    <aside id="leftsidebar" class="sidebar">
        <!-- User Info -->
        <div class="user-info">
            <div class="admin-image"> <img  src="{{asset('assets/images/random-avatar7.jpg')}}" alt=""> </div>
            <div class="admin-action-info"> <span>Hoş Geldiniz</span>
                <h3>@if(auth()->user()) {{auth()->user()->name}} @endif</h3>
                <ul>
                    <li>
                       @auth()
                            <form id="logout-form" action="{{ route('logout') }}" method="POST">
                                {{ csrf_field() }}
                                <button type="submit" class="btn btn-outline-primary text-white btn-sm fa fa-user" > Çıkış Yap</button>
                            </form>
                           @endauth
                        <br>
                    </li>
                </ul>
            </div>

        </div>
        <!-- #User Info -->
        <!-- Menu -->
        <div class="menu">
            <ul class="list">
                <li class="header">MENÜ</li>
                <li class="active open"><a href="/"><i class="zmdi zmdi-home"></i><span>Anasayfa</span></a></li>
                <li class=""><a href="/hastalar"><i class="fa fa-user "></i><span>Hastalar</span></a></li>
                <li><a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-account-o"></i><span>Bakım</span> </a>
                    <ul class="ml-menu">
                        <li><a href="{{route('bakim.bakılanlar')}}">Bakılanlar</a></li>
                        <li><a href="{{route('bakim.bakılmayanlar')}}">Bakılmayanlar</a></li>
                        <li><a href="{{route('bakim.tedavikabul')}}">Tedavi Kabul Edenler</a></li>
                        <li><a href="{{route('bakim.tedavired')}}">Tedavi Kabul Etmeyenler</a></li>
                        <li><a href="{{route('bakimlar.index')}}">Tüm Bakımlar</a></li>
                    </ul>
                </li>
                <li><a href="{{route('bakimlar.index')}}" class=""><i class="fa fa-eye"></i><span>Bakım</span> </a>

                </li>
                <li><a href="{{route("subeler.index")}}"><i class="zmdi zmdi-file-text"></i><span>Şubeler</span></a></li>
                 <li><a href="{{route('muhasebe.index')}}"><i class="fa fa-calculator"></i><span>Muhasebe</span></a></li>
                <li> <a href="{{route("kullanici.index")}}" class=""><i class="fa fa-user-secret"></i><span>Kullanıcılar</span> </a></li>
                <li> <a href="javascript:void(0);" class="menu-toggle"><i class="fa fa-gear"></i><span>Ayarlar (UI)</span> </a>
                    <ul class="ml-menu">
                        <li> <a href="{{route('okuma.index')}}">Okuma Ayarları</a> </li>
                        <li> <a href="{{route('temizlik.index')}}">Temizlik Ayarları</a></li>
                        <li> <a href="{{route('durum.index')}}">Durum Ayarları</a> </li>
                        <li> <a href="{{route('grup.index')}}">Grup Ayarları</a> </li>
                        <li> <a href="{{route('dokuman.index')}}">Doküman Ayarları</a> </li>

                    </ul>
                </li>

            </ul>
        </div>
        <!-- #Menu -->
    </aside>
    <!-- #END# Left Sidebar -->
    <!-- Right Sidebar -->

    <!-- #END# Right Sidebar -->
</section>
{{--İçerik alanı Başlabgıç--}}

@yield('content')
{{--İçerik alanı sonu--}}
<div class="color-bg"></div>
<!-- Jquery Core Js -->
<script src="{{asset('assets/bundles/libscripts.bundle.js')}}"></script> <!-- Lib Scripts Plugin Js -->
<script src="{{asset('assets/bundles/morphingsearchscripts.bundle.js')}}"></script> <!-- morphing search Js -->
<script src="{{asset('assets/bundles/vendorscripts.bundle.js')}}"></script> <!-- Lib Scripts Plugin Js -->
<script src="{{asset('assets/bundles/morphingscripts.bundle.js')}}"></script><!-- morphing search page js -->

<script src="{{asset('assets/plugins/jquery-sparkline/jquery.sparkline.min.js')}}"></script> <!-- Sparkline Plugin Js -->
<script src="{{asset('assets/plugins/chartjs/Chart.bundle.min.js')}}"></script> <!-- Chart Plugins Js -->

<script src="{{asset('assets/bundles/mainscripts.bundle.js')}}"></script><!-- Custom Js -->

<script src="{{asset('assets/js/pages/charts/sparkline.min.js')}}"></script>

@yield('js')
</body>
</html>
