<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Giriş </title>
    <link href="{{asset('assets/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
    <link rel="icon" href="{{asset('assets/images/Logoo/hatemhocalogo.png')}}" type="image/x-icon">
    <!-- Custom Css -->
    <link href="{{asset('assets/css/main.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/login.css')}}" rel="stylesheet">
    <style>
        body {
            font-family: 'Poppins', sans-serif; !important;
            font-size: 14px; !important;
        }
    </style>

    <!-- Swift Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="{{asset('assets/css/themes/all-themes.css')}}" rel="stylesheet" />
</head>
<body class="theme-cyan login-page authentication">

<div class="container">
    <div class="card">
        <p class="text-center">GİRİŞ YAP</p>
        <div class="col-md-12">
              <form method="POST" action="{{ route('login') }}">
                  @csrf
                  <div class="form-group row " style="margin-bottom: -5px">
                      <div class="col-md-12" >
                         <div class="form-line">
                             <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" placeholder="E- Mail" value="{{ old('email') }}" required autocomplete="email" autofocus>
                         </div>

                          @error('email')
                          <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                          @enderror
                      </div>
                  </div>
                  <div class="form-group row" style="margin-bottom: -5px">
                      <div class="col-md-12">
                         <div class="form-line">
                             <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="Şifre" required autocomplete="current-password">
                         </div>

                          @error('password')
                          <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                          @enderror
                      </div>
                  </div>

                  <div class="form-group row" style="margin-bottom: -5px">
                      <div class="col-md-12">
                          <div class="form-check">
                              <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                              <label class="form-check-label" for="remember">
                                  {{ __('Beni Hatırla') }}
                              </label>
                          </div>
                      </div>
                  </div>

                  <div class="form-group row mb-0" style="margin-top: -15px">
                      <div class="col-md-12 text-center">
                          <button type="submit" style="font-size: 16px" class="btn btn-lg btn-primary text-white">
                              {{ __('Giriş Yap') }}
                          </button>
                      </div>
                  </div>
              </form>
          </div>
    </div>
</div>
<div class="theme-bg"></div>

<!-- Jquery Core Js -->
<script src="{{asset('assets/bundles/libscripts.bundle.js')}}"></script> <!-- Lib Scripts Plugin Js -->
<script src="{{asset('assets/bundles/vendorscripts.bundle.js')}}"></script> <!-- Lib Scripts Plugin Js -->

<script src="{{asset('assets/bundles/mainscripts.bundle.js')}}"></script><!-- Custom Js -->
</body>
</html>
